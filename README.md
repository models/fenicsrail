[FEniCS]: https://fenicsproject.org/
[MORWIKI]: https://morwiki.mpi-magdeburg.mpg.de/morwiki/index.php/Steel_Profile
[BSD2im]: https://img.shields.io/badge/License-BSD%202--Clause-orange.svg
[BSD2]: https://opensource.org/licenses/BSD-2-Clause
[PYTHON]: https://www.python.org/downloads/
[NUMPY]: http://www.numpy.org/
[SCIPY]: http://www.scipy.org/
[CMESS]: https://gitlab.mpi-magdeburg.mpg.de/mess/cmess
[PARAVIEW]: https://www.paraview.org/
[FFMPEG]: https://www.ffmpeg.org/
[MATLAB]: https://de.mathworks.com/products/matlab.html
[DIPL]: http://www.mpi-magdeburg.mpg.de/mpcsc/saak/Data/diplom.ps.gz

# Reimplementation of Optimal Cooling Process for a Steel Profile of a Rail.

This repository is a reimplementation of the Rail benchmark example
described [here][MORWIKI].

**Version:** 2.0

**License:** [![License][BSD2im]][BSD2]

**Copyright:** 2018-2020 by Maximilian Behr, Jens Saak (MPI Magdeburg)

The implementation is done in [FEniCS][FEniCS].

*Documentation, Testing and Code update to the latest version of [FEniCS][FEniCS] is current work.*

# Software dependencies

 * [FEniCS][FEniCS] 2019.1.0
 * [Python][PYTHON] 3.6
 * [NumPy][NUMPY] and [SciPy][SCIPY]
 * Py-M.E.S.S. Python Interface for [C-M.E.S.S.][CMESS]
 * [Paraview][PARAVIEW] optional
 * [FFmpeg][FFMPEG] optional

# Installation, Testing, Documentation

tbd

# Code and File Descriptions

|       File/Folder                 |                   Description                                                     |
|:---------------------------------:|:---------------------------------------------------------------------------------:|
|   mesh                            | refined meshes with corresponding `MeshFunctions` and `VTK` dumps                 |
|   misc                            | information about [FEniCs][FEniCS] issues, which were helpful during development  |
|   misc/old_code                   | experimental code snippets which are not used                                     |
|   src/constants.py                | parameters, refinement algorithms, paths, integration methods                     |
|   src/railrefine.py               | functions for mesh refinement                                                     |
|   src/precomputation_ode.py       | simulates cooling at the air using ODE ansatz with boundary condition p.18 (1.3)  |
|   src/render.py                   | rendering using [Paraview][PARAVIEW]                                              |
|   src/renderall.sh                | script renders VTK dumps to avi files                                             |
|   src/raillog.py                  | logging class for project                                                         |
|   src/lqr_mixed.py                | simulates cooling at the air (Boundary condition p.18 (1.3))                      |
|   src/lqr_alt.py                  | simulates cooling at the air (Boundary condition p.18 (1.3))                      |
|   src/merge_videos.sh             | combine 2 or 4 videos for comparison                                              |

References in description column refer to [Diploma Thesis][DIPL].

# References and Model Desciption

For references [see][MORWIKI].

# License
The whole library is provided under the conditions of the BSD 2-Clause License. See [COPYING](COPYING) for details.
