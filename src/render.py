"""
Copyright (c) 2018-2020, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

import sys
import paraview.simple as para

# name of the label should be the same as in constants.py VTK_LABELNAME
VTK_LABEL_NAME = 'u'
# maximum temperature of the scalar bar widget
SCALE_MAX = 1.0
# minimal temperature of the scalar bar widget
SCALE_MIN = 0.45


def animate_rail(pvd_file, avi_file, title=''):
    """
    Function render video.

    Args:
        pvd_file         (String): points to the .pvd file which should be
                                   rendered
        avi_file         (String): name of the output avi file
        title            (String): optional, sets a title in video

    Returns:

    Raises:

    Information:
    Execute with pvpython provided by paraview.

    """

    para._DisableFirstRenderCameraReset()

    u_pvd = para.PVDReader(FileName=pvd_file)

    AnimationScene1 = para.GetAnimationScene()
    AnimationScene1.AnimationTime = 45.0
    AnimationScene1.Duration = 45
    AnimationScene1.PlayMode = 'Sequence'
    AnimationScene1.NumberOfFrames = 675

    RenderView1 = para.GetRenderView()
    RenderView1.CameraFocalPoint = [0.25, 0.8, 0.0]
    RenderView1.CameraPosition = [0.25, 0.8, 10000.0]
    RenderView1.InteractionMode = '2D'
    RenderView1.CenterOfRotation = [0.25, 0.8, 0.0]
    RenderView1.CenterAxesVisibility = 0
    RenderView1.OrientationAxesVisibility = 0

    DataRepresentation1 = para.Show()
    DataRepresentation1.EdgeColor = [0.0, 0.0, 0.5]
    DataRepresentation1.SelectionPointFieldDataArrayName = VTK_LABEL_NAME
    DataRepresentation1.ColorArrayName = ('POINT_DATA', VTK_LABEL_NAME)
    DataRepresentation1.ScalarOpacityUnitDistance = 0.2
    DataRepresentation1.ScaleFactor = 0.15

    a1_u_PVLookupTable = para.GetLookupTableForArray(VTK_LABEL_NAME,
                                                     1,
                                                     RGBPoints=[SCALE_MIN,
                                                                0.0,
                                                                0.0,
                                                                1.0,
                                                                SCALE_MAX,
                                                                1.0,
                                                                0.0,
                                                                0.0],
                                                     VectorMode='Magnitude',
                                                     NanColor=[0.25, 0.0, 0.0],
                                                     ColorSpace='HSV',
                                                     LockScalarRange=1)
    a1_u_PiecewiseFunction = para.CreatePiecewiseFunction(Points=[0.0, 0.0,
                                                                  0.5, 0.0,
                                                                  1.0, 1.0,
                                                                  0.5, 0.0])
    a1_u_PVLookupTable.ScalarOpacityFunction = a1_u_PiecewiseFunction
    DataRepresentation1.ScalarOpacityFunction = a1_u_PiecewiseFunction
    DataRepresentation1.LookupTable = a1_u_PVLookupTable

    ScalarBarWidgetRepresentation1 = para.CreateScalarBar(Title=VTK_LABEL_NAME,
                                                          LabelFontSize=12,
                                                          Enabled=1,
                                                          LookupTable=a1_u_PVLookupTable,
                                                          TitleFontSize=12)

    # render time
    # AnnotateTimeFilter1 = para.GetActiveSource()
    AnnotateTimeFilter2 = para.AnnotateTimeFilter()
    AnnotateTimeFilter2.Format = 'Time: %.1fs'
    RenderView1 = para.GetRenderView()
    # DataRepresentation3 = para.Show()
    DataRepresentation1 = para.GetDisplayProperties(u_pvd)
    # DataRepresentation2 = para.GetDisplayProperties(AnnotateTimeFilter1)

    # show text
    Text2 = para.Text()
    RenderView1 = para.GetRenderView()
    DataRepresentation5 = para.Show()
    u_pvd = para.FindSource("u.pvd")
    DataRepresentation1 = para.GetDisplayProperties(u_pvd)
    Text2.Text = para.title[0: len(title)/2] + "\n" + title[(len(title)+1)/2:
                                                            len(title)]
    DataRepresentation5.WindowLocation = 'UpperCenter'
    DataRepresentation5.FontSize = 12
    DataRepresentation5.FontFamily = 'Courier'

    para.GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)

    para.WriteAnimation(avi_file,
                        Magnification=1,
                        Quality=2,
                        FrameRate=15.000000)


if __name__ == "__main__":

    if not(len(sys.argv) == 3 or len(sys.argv) == 4):
        raise SyntaxError("Usage: ", sys.argv[0], " input.pvd  output.avi")

    if len(sys.argv) == 3:
        animate_rail(sys.argv[1], sys.argv[2])
    else:
        animate_rail(sys.argv[1], sys.argv[2], sys.argv[3])
