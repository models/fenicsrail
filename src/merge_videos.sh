#!/bin/sh

#Skript merges the 4 videos together to one using ffmpeg
#VIDEO1 TOP LEFT
#VIDEO2 TOP RIGHT
#VIDEO3 BOTTOM LEFT
#VIDEO4 BOTTOM RIGHT




VIDEO4="../Precomputation/ODE_ansatz_linear/1.2.0/regular_cut/1357_eulerimplicit/u.avi"


#iterate over all dir
NAMES=( ALT MIXED )
for NAME in "${NAMES[@]}"
do
    for DIR in /Users/daniels/Documents/LiClipseWorkspace/wip/LQR/LQR_${NAME}_linear/1.2.0/regular_cut/*
    do
        #echo $DIR
        #find all .avi files in each dir
        FIND_VID=`find ${DIR} -name *.avi`

        #convert to array
        idx=0
        for VID in $FIND_VID
        do
            VIDS[$idx]=${VID}
            idx=$(( idx + 1 ))
        done

        #echo ${VIDS[0]}
        #echo ${VIDS[1]}
        #echo ${VIDS[2]}

    FILENAME=${DIR}/${NAME}.mp4
    #echo ${FILENAME}
    #4 videos in one frame
    ffmpeg -i ${VIDS[0]} -i ${VIDS[1]} -i ${VIDS[2]} -i ${VIDEO4} -y -filter_complex "[0:0]pad=iw*2:ih*2[a];[a][1:0]overlay=w[b];[b][2:0]overlay=0:h[c];[c][3:0]overlay=w:h" -shortest ${FILENAME}
    done
done





#VIDEO1="../LQR/LQR_ALT_linear/1.3.0/regular_cut/371_eulerimplicit_LQR_C_60/u.avi"
#VIDEO2="../LQR/LQR_ALT_linear/1.3.0/regular_cut/371_eulerimplicit_LQR_C_TEMP/u.avi"
#VIDEO3="../LQR/LQR_ALT_linear/1.3.0/regular_cut/371_eulerimplicit_LQR_C_DIFF/u.avi"
#VIDEO4="../Precomputation/ODE_ansatz_linear/1.3.0/regular_cut/371_eulerimplicit/u.avi"

#side by side with two videos
#ffmpeg -i ${VIDEO1} -vf "movie=${VIDEO2} [in1]; [in]pad=iw*2:ih:iw:0[in0]; [in0][in1] overlay=0:0 [out]" sidebyside.avi

#4 videos in one frame
#ffmpeg -i ${VIDEO1} -i ${VIDEO2} -i ${VIDEO3} -i ${VIDEO4} -filter_complex "[0:0]pad=iw*2:ih*2[a];[a][1:0]overlay=w[b];[b][2:0]overlay=0:h[c];[c][3:0]overlay=w:h" -shortest 371_alt.mp4
