"""
Copyright (c) 2018-2020, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

import os
import dolfin as df
import numpy as np
from scipy.io import savemat
import constants as CONST


def ODE_assemble(mesh_file, gamma_numbers_file):
    """
    Function assembles ODE-System like in 2.2 Schwache Formulierung und
    Diskretisierung des Waermeleitungsgleichungsproblems p.26/27
    M\dot{u}=-(S*M_GAMMA)u+Bq

     Args:
       mesh_file           (String):  points to the .xml mesh file
                                      (mesh folder)
       gamma_numbers_file  (String):  points to the .xml MeshFunction
                                      file (mesh folder)

    Returns:

    Raises:

    Information:

    """

    # set matrix reordering off
    df.parameters['reorder_dofs_serial'] = False

    # read mesh file and mesh function for gamma numbers
    mesh = df.Mesh(mesh_file)
    gamma_numbers = df.MeshFunction('size_t', mesh, gamma_numbers_file)

    # define constants
    const_rho = df.Constant(CONST.CONST_RHO)
    const_c = df.Constant(CONST.CONST_C)
    const_gammak = df.Constant(CONST.CONST_GAMMAK)
    const_alpha = df.Constant(CONST.CONST_ALPHA)
    const_u_ext = df.Constant(CONST.CONST_UEXT)

    # define function space linear lagrange elements P1
    V = df.FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    # temperature, time derivative, and test function
    u = df.TrialFunction(V)
    du_dt = df.TrialFunction(V)
    v = df.TestFunction(V)

    # system weak formulation
    ds = df.Measure('ds')
    ds = ds(subdomain_data=gamma_numbers)
    M_var = du_dt * v * df.dx
    S_var = const_alpha * df.inner(df.grad(u), df.grad(v)) * df.dx
    M_GAMMA_var = sum([const_gammak / (const_c * const_rho) * u * v * ds(i)
                       for i in range(1, 8)])
    B_var = sum([const_gammak / (const_c * const_rho) * v * ds(i)
                 for i in range(1, 8)])
    M_GAMMA_vars = [const_gammak / (const_c * const_rho) * u * v * ds(i)
                    for i in range(1, 8)]
    B_vars = [const_gammak / (const_c * const_rho) * v * ds(i)
              for i in range(1, 8)]

    # assemble system M\dot{u}= -(S+M_GAMMA)*u+B
    M = df.assemble(M_var)
    S = df.assemble(S_var)
    M_GAMMA = df.assemble(M_GAMMA_var)
    B = df.assemble(B_var)
    M_GAMMAs = [df.assemble(x) for x in M_GAMMA_vars]
    B_s = [df.assemble(x) for x in B_vars]

    # save assembled matrices in a mat file
    filename = CONST.ODE_ASSEMBLE_MAT_FILENAME % (mesh.num_vertices())

    M_csr = CONST.get_sparsedata(M)
    M_csr.eliminate_zeros()
    S_csr = CONST.get_sparsedata(S)
    S_csr.eliminate_zeros()
    M_GAMMA_csr = CONST.get_sparsedata(M_GAMMA)
    M_GAMMA_csr.eliminate_zeros()

    mydict = {'M': M_csr, 'S': S_csr, 'M_GAMMA': M_GAMMA_csr, 'B': np.array(B)}

    for i in range(0, len(M_GAMMAs)):
        M_GAMMA_tmp = CONST.get_sparsedata(M_GAMMAs[i])
        M_GAMMA_tmp.eliminate_zeros()
        mydict['M_GAMMA_' + str(i)] = M_GAMMA_tmp

    for i in range(0, len(B_s)):
        mydict['B_' + str(i)] = np.array(B_s[i])

    if not os.path.exists(CONST.ODE_ASSEMBLE_FOLDER):
        os.makedirs(CONST.ODE_ASSEMBLE_FOLDER)
    savemat(filename, mdict=mydict)


if __name__ == "__main__":

    print("---------------------- assemble ODE Systems ----------------------")
    ODE_assemble(CONST.MACRO_MESH_FILE, CONST.MACRO_GAMMA_NUMBERS_FILE)
    for i in range(1, 9):
        ODE_assemble(CONST.REFN_MESH_FILE(i), CONST.REFN_GAMMA_NUMBERS_FILE(i))
