"""
Copyright (c) 2018, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

# -*- coding: utf-8 -*-
from dolfin import (parameters, Constant, Mesh, MeshFunction, Measure,
                    Function, TrialFunction, TestFunction, inner, solve, grad,
                    assemble, dx, FunctionSpace, project, File,
                    vertex_to_dof_map, dof_to_vertex_map)
from pymess import lrnm, equations, options
from scipy.linalg import cholesky, solve_triangular, cho_solve
from scipy.io import savemat
import numpy as np
import constants as CONST
import raillog


def LQR_MIXED_linear(mesh_file, gamma_numbers_file, integrator, C_func,
                     LQR_COST):
    """
    LQR problem solver (3.1.1 p.33) (4.3.1  p.50) with mixed boundary condition

    For simulation precomputation using p.18 equation(1.3) q_k=kappa,
    For cooling using p.18 equation(1.4).

    Use constants coefficents rho, c, lambda.

    Function takes the constants from wip_constants, define a weak formulation
    and simulate cooling process until maximum temperature is lower than
    CONST_U_INIT
    (defined in constants.py).
    Then the optimal feedback is computed by solving the Riccatti Equation from
    the LQR Problem with the mess python interface.

    Save temperature in vtk file and temperature at the end of time in mat file
    see constants.py

     Args:
        mesh_file          (String):     points to the .xml mesh file
                                         (mesh folder)
        gamma_numbers_file (String):     points to the .xml MeshFunction file
                                         (mesh folder)
        integrator         (String):     CONST.EULER_IMPLICIT,
                                         CONST.EULER_EXPLICIT,
                                         CONST.CRANK_NICOLSON
        C_func             (Function):   return Output Matrix C for given
                                         Number of Vertices
                                         (s.a. constants.py)
        LQR_COST           (dictionary): s.a. constants.py



    Returns:

    Raises:

    Information:

    """

    parameters['reorder_dofs_serial'] = False

    # define constants
    const_rho = Constant(CONST.CONST_RHO)
    const_c = Constant(CONST.CONST_C)
    const_alpha = Constant(CONST.CONST_ALPHA)
    const_kappa = CONST.CONST_KAPPA
    const_u_ext = CONST.CONST_UEXT
    const_gammak = CONST.CONST_GAMMAK

    # read mesh and MeshFunction for gamma boundary
    mesh = Mesh(mesh_file)
    gamma_numbers = MeshFunction('size_t', mesh, gamma_numbers_file)

    # define function space linear lagrange elements P1
    # define u0 as constant temperature and project u_0 down to Function Space
    V = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)
    u_0 = Constant(CONST.CONST_U0)
    u_k_1 = project(u_0, V)
    u_k_1.rename(CONST.VTK_FIELD_NAME, CONST.VTK_LABEL_NAME)
    u_k = Function(V)

    # define time step size, simulation time and t for iterating over time
    delta_t = CONST.T_DELTA
    T = CONST.T_END
    t = delta_t

    # solution u and time derivative du_dt
    u = TrialFunction(V)
    du_dt = TrialFunction(V)
    v = TestFunction(V)

    # weak formulation, B_var is for the "Anlaufrechnung", B_vars for Feedback
    ds = Measure('ds')
    ds = ds(subdomain_data=gamma_numbers)
    M_var = du_dt * v * dx
    S_var = const_alpha * inner(grad(u), grad(v)) * dx
    M_GAMMA_var = sum([1 / (const_c * const_rho) * u * v * ds(i)
                       for i in range(1, 8)])
    B_var = sum([const_kappa * const_u_ext / (const_c * const_rho) * v * ds(i)
                 for i in range(1, 8)])
    B_vars = [const_u_ext / (const_c * const_rho) * v * ds(i)
              for i in range(1, 8)]

    # assemble matrices, B_vec is for "Anlaufrechnung", B_cols/B_mat is for
    # Feedback
    M = assemble(M_var)
    S = assemble(S_var)
    M_GAMMA = assemble(M_GAMMA_var)
    B_vec = assemble(B_var)
    B_cols = [assemble(x) for x in B_vars]

    # dense matrix from B_cols
    B_mat = np.array(B_cols)

    # set correct matrices for iteration
    # M_iter*u_k=A_iter*u_k_1+delta_t*B
    if integrator == CONST.EULER_IMPLICIT:
        M_iter = M + delta_t * (S + const_kappa * M_GAMMA)
        A_iter = M
        M_iter_feed = M + delta_t * (S + const_gammak * M_GAMMA)
        A_iter_feed = M
    elif integrator == CONST.EULER_EXPLICIT:
        M_iter = M
        A_iter = M - delta_t * (S + const_kappa * M_GAMMA)
        M_iter_feed = M
        A_iter_feed = M - delta_t * (S + const_gammak * M_GAMMA)
    elif integrator == CONST.CRANK_NICOLSON:
        M_iter = M + 1 / 2 * delta_t * (S + const_kappa * M_GAMMA)
        A_iter = M - 1 / 2 * delta_t * (S + const_kappa * M_GAMMA)
        A_iter_feed = M - 1 / 2 * delta_t * (S + const_gammak * M_GAMMA)
        M_iter_feed = M + 1 / 2 * delta_t * (S + const_gammak * M_GAMMA)
    else:
        raise ValueError('integration method: %s is not implemented' %
                         (integrator))

    # directory for output
    dirname = (LQR_COST.name + '/' + str(mesh.num_vertices()) + '_' +
               integrator + '_' + C_func.__name__)

    # define vtk file
    vtk_file = File(CONST.LQR_MIXED_LINEAR_VTK_FILENAME % dirname)
    vtk_file << u_k_1

    # add file handler to raillogger
    raillog.add_file_handler(CONST.LQR_MIXED_LINEAR_LOG % dirname)
    raillog.log(__file__ + "-LQR_MIXED_linear")
    raillog.log("mesh_file=%s" % mesh_file)
    raillog.log("gamma_numbers_file=%s" % gamma_numbers_file)
    raillog.log("integrator=%s" % integrator)
    raillog.log("C=%s" % C_func.__name__)

    # add file writer for controls q and states y and write to file
    control_log = open(CONST.LQR_MIXED_LINEAR_CONTROL_LOG % dirname, 'w')
    y_log = open(CONST.LQR_MIXED_LINEAR_OUTPUT_LOG % dirname, 'w')
    control_log.write(CONST.LQR_MIXED_LINEAR_CONTROL_LOG_HEADER)
    y_log.write(CONST.LQR_MIXED_LINEAR_OUTPUT_LOG_HEADER)

    # progress and vtk sample time
    old_percent = -CONST.PERCENT_STEP
    old_time = -CONST.VTK_SAMPLE_TIME
    u_max = 1

    # ########## start  simulation ######### #
    while CONST.CONST_U_INIT < u_max and t < T:

        # compute rhs and solve equation system
        b = A_iter * u_k_1.vector() + delta_t * B_vec
        solve(M_iter, u_k.vector(), b, CONST.LINEAR_SOLVER)

        # print debug information if percent difference is large enough
        if (t - old_time) >= CONST.VTK_SAMPLE_TIME:
            raillog.log(CONST.VTK_OUTPUT % (t / 100))
            vtk_file << u_k_1
            old_time = t

        # print information if percent difference is large enough
        percent = (t / T) * 100
        if (percent - old_percent) >= CONST.PERCENT_STEP:
            raillog.log(CONST.PERCENT_OUTPUT % (percent, t / 100,
                                                u_k.vector().max(),
                                                u_k.vector().min()))
            old_percent = percent

        # increment time and u_k_1<-u_k
        t += delta_t
        u_max = u_k.vector().max()
        u_k_1.assign(u_k)
    # ########## end simulation ######### #

    # ########## solve ARE ######### #
    # options for ARE
    opt = options.Options()
    opt.adi.type = 'C'        # MESS_OP_TRANSPOSE
    opt.adi.output = CONST.PYMESS_ADI_OUTPUT
    opt.nm.output = CONST.PYMESS_NM_OUTPUT

    # Output Matrix C with dof reordering
    # set Q and R correct in ARE and convert to matrix
    C = C_func(mesh.num_vertices(), vertex_to_dof_map(V))
    Q_chol_L = cholesky(LQR_COST.Q, lower=True)
    R_chol_L = cholesky(LQR_COST.R, lower=True)

    # set up Matrices for ARE p.41 p.26 (2.21)
    eqn = equations.std.EquationGRiccati(
        opt,
        CONST.get_sparsedata(-1 * S - const_gammak * M_GAMMA),
        CONST.get_sparsedata(M),
        solve_triangular(R_chol_L, B_mat, 'N', lower=True).T,
        np.dot(Q_chol_L.T, C))

    # call lrnm and get low rank solution factor
    Z, stat = lrnm(eqn, opt)
    raillog.log("LRNM ARE Res:%E" % stat.res2_norm)
    BT_Z = np.dot(B_mat, Z)
    # ########### end solve ARE ########## #

    # ######### start controlled cooling ######### #
    while t < T:

        # control q (numpy array) by put last temperature in feedback
        ZTu_k_1 = np.dot(Z.T, np.array(M * u_k_1.vector(), ndmin=2).T)
        q = -1 * cho_solve((R_chol_L, True), np.dot(BT_Z, ZTu_k_1))

        # compute new right hand side
        b[:] = np.dot(delta_t * B_mat.T, q).ravel()[:]
        b = A_iter_feed * u_k_1.vector() + b
        solve(M_iter_feed, u_k.vector(), b,
              CONST.LINEAR_SOLVER,
              CONST.PRECONDITIONER)

        # print debug information if percent difference is large enough
        if (t - old_time) >= CONST.VTK_SAMPLE_TIME:
            raillog.log(CONST.VTK_OUTPUT % (t / 100))
            vtk_file << u_k_1
            old_time = t

        # print information if percent difference is large enough
        percent = (t / T) * 100
        if (percent - old_percent) >= CONST.PERCENT_STEP:
            raillog.log(CONST.PERCENT_OUTPUT % (percent, t / 100,
                                                u_k.vector().max(),
                                                u_k.vector().min()))
            old_percent = percent

            # compute cost functional
            costq = np.dot(eqn.c, u_k_1.vector())
            costq = np.squeeze(np.asarray(costq))
            costq = np.dot(costq, costq.T)
            costr = np.dot(LQR_COST.R, q)
            costr = np.dot(q.T, costr)
            raillog.log(CONST.PERCENT_COST_OUTPUT % (costq, costr))

            # write control q and output y to file
            control_log.write(CONST.LQR_MIXED_LINEAR_CONTROL_LOG_FMT(q))
            y = np.dot(eqn.c, u_k_1.vector())
            y = np.squeeze(np.asarray(y)).tolist()
            y_log.write(CONST.LQR_MIXED_LINEAR_OUTPUT_LOG_FMT(y))

        # increment time and u_k_1<-u_k
        t += delta_t
        u_k_1.assign(u_k)
    raillog.log(CONST.PERCENT_OUTPUT % (percent, t / 100,
                                        u_k_1.vector().max(),
                                        u_k_1.vector().min()))
    # ######### end controlled cooling ######### #

    # save result
    u_file = CONST.LQR_MIXED_LINEAR_MAT_FILENAME % dirname
    savemat(u_file, mdict={'u_end': np.array(u_k.vector()),
                           'T': T,
                           'delta_t': delta_t,
                           'integrator': integrator,
                           'Q': LQR_COST.Q,
                           'R': LQR_COST.R,
                           'C': C,
                           'C_name': C_func.__name__,
                           'doftovertexmap': dof_to_vertex_map(V),
                           'vertextodofmap': vertex_to_dof_map(V)})
    raillog.log("Save data in %s" % u_file)

    # remove file_handler from raillogger, close control_log and y_log
    raillog.remove_file_handlers_handlers()
    control_log.close()
    y_log.close()


if __name__ == '__main__':

    for (wR, wQ) in [(1, 1), (1, 1e1), (1e1, 1), (1, 1e4), (1e4, 1), (1, 1e7),
                     (1e7, 1)]:
        for i in range(2, 3):
            LQR_MIXED_linear(CONST.REFN_MESH_FILE(i),
                             CONST.REFN_GAMMA_NUMBERS_FILE(i),
                             CONST.EULER_IMPLICIT,
                             CONST.LQR_C_60,
                             CONST.LQR_COST(wR, wQ))
            LQR_MIXED_linear(CONST.REFN_MESH_FILE(i),
                             CONST.REFN_GAMMA_NUMBERS_FILE(i),
                             CONST.EULER_IMPLICIT,
                             CONST.LQR_C_TEMP,
                             CONST.LQR_COST(wR, wQ))
            LQR_MIXED_linear(CONST.REFN_MESH_FILE(i),
                             CONST.REFN_GAMMA_NUMBERS_FILE(i),
                             CONST.EULER_IMPLICIT,
                             CONST.LQR_C_DIFF,
                             CONST.LQR_COST(wR, wQ))
