"""
Copyright (c) 2018, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

from scipy.io import savemat
from dolfin import (Constant, Measure, Mesh, MeshFunction, Function,
                    TrialFunction, TestFunction, inner, grad, dx,
                    FunctionSpace, project, solve, File,
                    vertex_to_dof_map, dof_to_vertex_map)
import raillog
import constants as CONST


def PDE_ansatz_linear(mesh_file, gamma_numbers_file, integrator):
    """Precomputation (4.2 Die ungesteuerte Referenzrechnung) p.47. with
    PDE-Ansatz.
    Boundary condition p.18 (1.3) with q_k == kappa.
    Use constants coefficients rho, c, lambda.

    Function takes the constants from wip_constants, define a weak formulation
    and solve it with PDE-Ansatz

    Function takes the constants from wip_constants, define a weak formulation
    and solve it with PDE-Ansatz and numerical integration.
    Save temperature in vtk file and temperature at the end in mat file see
    constants.py

     Args:
        mesh_file          (String):  points to the .xml mesh file
                                      (mesh folder)
        gamma_numbers_file (String):  points to the .xml MeshFunction file
                                      (mesh folder)
        integrator         (String):  CONST.EULER_IMPLICIT,
                                      CONST.EULER_EXPLICIT,
                                      CONST.CRANK_NICOLSON

    Returns:

    Raises:

    Information:
    In the diploma thesis p.51 is written that system is resistant due to
    control the cooling process with mixed boundary condition.

    """

    # set constants
    const_rho = Constant(CONST.CONST_RHO)
    const_c = Constant(CONST.CONST_C)
    const_kappa = Constant(CONST.CONST_KAPPA)
    const_alpha = Constant(CONST.CONST_ALPHA)
    const_u_ext = Constant(CONST.CONST_UEXT)

    # read mesh and gamma_numbers
    mesh = Mesh(mesh_file)
    gamma_numbers = MeshFunction('size_t', mesh, gamma_numbers_file)

    # define function space linear Lagrange P1
    V = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    # set time step size and simulation time and t for iterating over time
    delta_t = CONST.T_DELTA
    T = CONST.T_END
    t = delta_t

    # the solution u
    u = TrialFunction(V)
    v = TestFunction(V)

    # define u0 as constant temperature
    u_0 = Constant(CONST.CONST_U0)

    # project u_0 down to Function Space V
    u_k_1 = project(u_0, V)

    # define terms of weak formulations
    ds = Measure('ds')
    ds = ds(subdomain_data=gamma_numbers)
    if integrator == CONST.EULER_IMPLICIT:
        # define bilinear form
        a = u * v * dx + delta_t * const_alpha * inner(grad(u), grad(v)) * dx
        a += sum([delta_t * const_kappa / (const_c * const_rho) * u * v * ds(i)
                  for i in range(1, 8)])

        # define linear form (right hand side)
        L = u_k_1 * v * dx
        L += sum([delta_t * const_kappa / (const_c * const_rho) * const_u_ext *
                  v * ds(i) for i in range(1, 8)])
    elif integrator == CONST.EULER_EXPLICIT:
        # define bilinear form
        a = u*v*dx

        # define linear form (right hand side)
        L = u_k_1*v*dx - delta_t*const_alpha*inner(grad(u_k_1), grad(v))*dx
        L += sum([delta_t * (const_kappa / (const_c * const_rho)) *
                  (const_u_ext-u_k_1) * v * ds(i) for i in range(1, 8)])

    elif integrator == CONST.CRANK_NICOLSON:
        # define bilinear form
        a = u * v * dx + \
            delta_t * const_alpha * 0.5 * inner(grad(u), grad(v)) * dx
        a += sum([0.5 * delta_t * const_kappa / (const_c * const_rho) * u*v *
                  ds(i) for i in range(1, 8)])

        # define linear form (right hand side)
        L = u_k_1 * v * dx - \
            delta_t * 0.5 * const_alpha * inner(grad(u_k_1), grad(v)) * dx
        L += sum([delta_t * (const_kappa / (const_c * const_rho)) *
                  const_u_ext * v * ds(i) for i in range(1, 8)])
        L -= sum([0.5 * delta_t * (const_kappa / (const_c * const_rho)) *
                  u_k_1 * v * ds(i) for i in range(1, 8)])

    else:
        raise ValueError('integration method: %s is not implemented' %
                         (integrator))

    # the solution
    u = Function(V)

    # set name in vtk file
    u_k_1.rename(CONST.VTK_FIELD_NAME, CONST.VTK_LABEL_NAME)

    # file for visualization
    vtk_file = File(CONST.PDE_ANSATZ_LINEAR_VTK_FILENAME %
                    (str(mesh.num_vertices()) + '_' + integrator),
                    'compressed')
    vtk_file << u_k_1

    # get logger
    raillog.add_file_handler(CONST.PDE_ANSATZ_LINEAR_LOG %
                             (str(mesh.num_vertices()) + '_' + integrator))
    raillog.log(__file__+"-PDE_ansatz_linear")
    raillog.log("mesh_file=%s" % mesh_file)
    raillog.log("gamma_numbers_file=%s" % gamma_numbers_file)
    raillog.log("integrator=%s" % integrator)

    # debug info
    old_percent = -CONST.PERCENT_STEP

    # time between two vtk sample
    old_time = -CONST.VTK_SAMPLE_TIME

    # start simulation
    while t <= T:
        # solve pde
        solve(a == L, u)

        # write vtk dump if time difference is large enough
        if(t - old_time) >= CONST.VTK_SAMPLE_TIME:
            raillog.log(CONST.VTK_OUTPUT % (t / 100))
            vtk_file << u_k_1
            old_time = t

        # print debug information if percent difference is large enough
        percent = (t / T) * 100
        if (percent-old_percent) >= CONST.PERCENT_STEP:
            raillog.log(CONST.PERCENT_OUTPUT %
                        (percent, t / 100, u.vector().max(), u.vector().min()))
            old_percent = percent

        # increment time and u_k_1<-u_k
        t += delta_t
        u_k_1.assign(u)
    # end simulation

    raillog.log(CONST.PERCENT_OUTPUT % (percent,
                                        t/100,
                                        u.vector().max(),
                                        u.vector().min()))

    # save result
    u_file = CONST.PDE_ANSATZ_LINEAR_MAT_FILENAME % \
        (str(mesh.num_vertices()) + '_' + integrator)
    savemat(u_file, mdict={'u_end': u.vector().array(),
                           'T': T,
                           'delta_t': delta_t,
                           'integrator': integrator,
                           'doftovertexmap': dof_to_vertex_map(V),
                           'vertextodofmap': vertex_to_dof_map(V)})

    raillog.log("Save data in %s" % u_file)

    # remove file_handler from railloger
    raillog.remove_file_handlers_handlers()


def PDE_ansatz_nonlinear(mesh_file, gamma_numbers_file, integrator):
    """Precomputation (4.2 Die ungesteuerte Referenzrechnung) p.47. via with
    PDE-Ansatz with nonlinear coefficients
    Boundary condition p.18 (1.3) with q_k == kappa.
    Use linear coefficents rho, c, lambda.

    Function takes the constants from wip_constants, define a weak formulation
    and solve it with PDE-Ansatz and numerical integration.
    Save temperature in VTK file and temperature at the end in mat file see
    constants.py

     Args:
        mesh_file          (String):  points to the .xml mesh file
                                      (mesh folder)
        gamma_numbers_file (String):  points to the .xml MeshFunction file
                                      (mesh folder)
        integrator         (String):  CONST.EULER_IMPLICIT,
                                      CONST.EULER_EXPLICIT,
                                      CONST.CRANK_NICOLSON

    Returns:

    Raises:

    Information:

    """

    # set constants or linear functions
    rho = CONST.LINEAR_RHO
    c = CONST.LINEAR_C
    const_kappa = Constant(CONST.CONST_KAPPA)
    alpha = CONST.LINEAR_ALPHA
    const_u_ext = Constant(CONST.CONST_UEXT)

    # read mesh and gamma_numbers
    mesh = Mesh(mesh_file)
    gamma_numbers = MeshFunction('size_t', mesh, gamma_numbers_file)

    # define function space linear Lagrange P1
    V = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    # set time step size and simulation time and t for iterating over time
    delta_t = CONST.T_DELTA
    T = CONST.T_END
    t = delta_t

    # the solution u^{k}
    u = TrialFunction(V)
    v = TestFunction(V)

    # define u0 as constant temperature
    u_0 = Constant(CONST.CONST_U0)

    # project u_0 down to Function Space V
    u_k_1 = project(u_0, V)

    # define terms of weak formulations
    ds = Measure('ds')
    ds = ds(subdomain_data=gamma_numbers)
    if integrator == CONST.EULER_IMPLICIT:
        # define bilinear form
        a = u * v * dx + \
            delta_t * alpha(u_k_1) * inner(grad(u), grad(v)) * dx
        a += sum([delta_t * const_kappa / (c(u_k_1) * rho(u_k_1)) *
                  u*v * ds(i) for i in range(1, 8)])

        # define linear form (right hand side)
        L = u_k_1 * v * dx
        L += sum([delta_t * const_kappa / (c(u_k_1) * rho(u_k_1)) *
                  const_u_ext * v * ds(i) for i in range(1, 8)])
    elif integrator == CONST.EULER_EXPLICIT:
        # define bilinear form
        a = u * v * dx

        # define linear form (right hand side)
        L = u_k_1 * v * dx - \
            delta_t * alpha(u_k_1) * inner(grad(u_k_1), grad(v)) * dx
        L += sum([delta_t * (const_kappa / (c(u_k_1) * rho(u_k_1))) *
                  (const_u_ext - u_k_1) * v * ds(i) for i in range(1, 8)])

    elif integrator == CONST.CRANK_NICOLSON:
        # define bilinear form
        a = u * v * dx + \
            delta_t * alpha(u_k_1) * 0.5 * inner(grad(u), grad(v)) * dx
        a += sum([0.5 * delta_t * const_kappa / (c(u_k_1) * rho(u_k_1)) *
                  u*v * ds(i) for i in range(1, 8)])

        # define linear form (right hand side)
        L = u_k_1 * v * dx - \
            delta_t * 0.5 * alpha(u_k_1) * inner(grad(u_k_1), grad(v)) * dx
        L += sum([delta_t * (const_kappa / (c(u_k_1) * rho(u_k_1))) *
                  const_u_ext * v * ds(i) for i in range(1, 8)])
        L -= sum([0.5 * delta_t * (const_kappa / (c(u_k_1) * rho(u_k_1))) *
                  u_k_1*v*ds(i) for i in range(1, 8)])

    else:
        raise ValueError('integration method: %s is not implemented' %
                         (integrator))

    # the solution
    u = Function(V)

    # set name in vtk file
    u_k_1.rename(CONST.VTK_FIELD_NAME, CONST.VTK_LABEL_NAME)

    # file for visualization
    vtk_file = File(CONST.PDE_ANSATZ_NONLINEAR_VTK_FILENAME %
                    (str(mesh.num_vertices()) + '_' + integrator),
                    'compressed')

    # debug info
    old_percent = -CONST.PERCENT_STEP

    # time between two vtk sample
    old_time = -CONST.VTK_SAMPLE_TIME

    # add file handler to raillogger
    raillog.add_file_handler(CONST.PDE_ANSATZ_NONLINEAR_LOG %
                             (str(mesh.num_vertices()) + '_' + integrator))
    raillog.log(__file__ + "-PDE_ansatz_nonlinear")
    raillog.log("mesh_file=%s" % mesh_file)
    raillog.log("gamma_numbers_file=%s" % gamma_numbers_file)
    raillog.log("integrator=%s" % integrator)

    # start simulation
    while t <= T:
        # solve pde
        solve(a == L, u)

        # check time difference for VTK sample
        if (t-old_time) >= CONST.VTK_SAMPLE_TIME:
            raillog.log(CONST.VTK_OUTPUT % (t / 100))
            vtk_file << u_k_1
            old_time = t

        # check progress for output information
        percent = (t / T) * 100
        if (percent - old_percent) >= CONST.PERCENT_STEP:
            raillog.log(CONST.PERCENT_OUTPUT % (percent,
                                                t/100,
                                                u.vector().max(),
                                                u.vector().min()))
            old_percent = percent

        t += delta_t
        u_k_1.assign(u)
    # end simulation

    raillog.log(CONST.PERCENT_OUTPUT % (percent,
                                        t/100,
                                        u.vector().max(),
                                        u.vector().min()))

    # save result
    u_file = CONST.PDE_ANSATZ_NONLINEAR_MAT_FILENAME % \
        (str(mesh.num_vertices()) + '_' + integrator)
    savemat(u_file, mdict={'u_end': u.vector().array(),
                           'T': T,
                           'delta_t': delta_t,
                           'integrator': integrator,
                           'doftovertexmap': dof_to_vertex_map(V),
                           'vertextodofmap': vertex_to_dof_map(V)})

    raillog.log('Save data in %s' % u_file)

    # remove file_handler from railloger
    raillog.remove_file_handlers_handlers()


if __name__ == "__main__":

    integrators = [CONST.EULER_IMPLICIT]
    for integrator in integrators:
        for i in range(2, 3):
            print("-------------------------- ref%d --------------------------"
                  % i)
            PDE_ansatz_linear(CONST.REFN_MESH_FILE(i),
                              CONST.REFN_GAMMA_NUMBERS_FILE(i),
                              integrator)
            PDE_ansatz_nonlinear(CONST.REFN_MESH_FILE(i),
                                 CONST.REFN_GAMMA_NUMBERS_FILE(i),
                                 integrator)
