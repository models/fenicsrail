"""
Copyright (c) 2018, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

# -*- coding: utf-8 -*-
import logging
import constants as CONST

datefmt = CONST.RAILLOG_datefmt
fmt = CONST.RAILLOG_fmt
railloglevel = CONST.RAILLOG_level


def add_console_handler():
    console = logging.StreamHandler()
    console.setLevel(railloglevel)
    console_formatter = logging.Formatter(CONST.RAILLOG_fmt)
    console_formatter.datefmt = CONST.RAILLOG_datefmt
    console.setFormatter(console_formatter)
    raillogger.addHandler(console)


def add_file_handler(filename_):
    filehandler = logging.FileHandler(filename_, 'w')
    filehandler.setLevel(railloglevel)
    filehandler_formatter = logging.Formatter(CONST.RAILLOG_fmt)
    filehandler_formatter.datefmt = CONST.RAILLOG_datefmt
    filehandler.setFormatter(filehandler_formatter)
    raillogger.addHandler(filehandler)


def remove_file_handlers_handlers():
    handlers = raillogger.handlers
    for handler in handlers:
        if handler.__class__ == logging.FileHandler:
            raillogger.removeHandler(handler)


def log(msg):
    raillogger.log(railloglevel, msg)


# add console_handler to raillogger
raillogger = logging.getLogger(__name__)
add_console_handler()
