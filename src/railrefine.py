"""
Copyright (c) 2018, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

# -*- coding: utf-8 -*-
import numpy as np
from dolfin import (MeshFunction, Mesh, refine, File, Vertex, edges, cells,
                    parameters, vertices)
import constants as CONST


def edge_on_boundary(edge):
    """Return True if an edge is on boundary

    Function counts the number of incident cells to the edge, if sum is one
    it returns True otherwise False.
    Args:
        edge        (Edge):            edge for checking if is on boundary

    Returns:
        True if edge on boundary of mesh otherwise false

    Information:
        You probably have to call mesh.init(1,2) before using edge_on_boundary
        for initializing an edge cell relation.

    """
    return sum(1 for cell in cells(edge)) == 1


def compute_boundary_parent_edge_function(mesh, ref_mesh):
    """Get mesh and refined mesh (ref_mesh) as argument and return a
    MeshFunction defined on the edges of ref_mesh which is -1 is edge is an
    inner edge or returns the parent edge indices otherwise.

    Assumption is that if a new vertices of ref_mesh, which are not in mesh
    have an index higher than mesh.num_vertices()-1
    Args:
        mesh        (Mesh):            the mesh
        ref_mesh    (Mesh):            the refined mesh of argument mesh

    Returns:
        boundary_parent_edge (MeshFunction): MeshFunction defined on the edges
        of ref_mesh returns -1 if Edge is an inner edge
        if edge is on boundary it returns the index of the parent edge in mesh
        (this edge in mesh is obviously also on the boundary of mesh).

    Raises:
        ValueError: if algorithm is not able to determine the parent edge or
                    if an edge on boundary of ref_mesh has more than one new
                    vertices, than the edge in mesh.

    Information:
        Code was tested with regular_cut and recursive_bisection refined
        meshes.
    """

    # the highest index in mesh
    maxvertind = mesh.num_vertices()-1

    # init all edges with -1 indicates that edge is an inner edge
    boundary_parent_edge = MeshFunction('int', ref_mesh, 1, -1)

    # compute vertex edge relation
    mesh.init(0, 1)
    # compute edge cell relation
    ref_mesh.init(1, 2)
    # compute vertex edge relation
    ref_mesh.init(0, 1)

    for eref in edges(ref_mesh):
        if edge_on_boundary(eref):
            # entry is true if vertex is also in mesh otherwise false
            are_verts_old = eref.entities(0)[:] <= maxvertind

            if all(are_verts_old):
                # both vertices are also in mesh
                # find parent edge from the two vertices indices
                v0ind = eref.entities(0)[0]
                v1ind = eref.entities(0)[1]
                v0 = Vertex(mesh, v0ind)
                v1 = Vertex(mesh, v1ind)
                # get edge indices of the connecting edge which is incident to
                # v0 and v1
                # arrays should be unique
                parent_edgeind = np.intersect1d(v0.entities(1), v1.entities(1),
                                                True)
                if parent_edgeind.size == 1:
                    # set parent edge
                    boundary_parent_edge[eref] = parent_edgeind[0]
                else:
                    raise ValueError("Could not construct parent edge in mesh "
                                     + "from vertices with indices %d, %d" %
                                     (v0ind, v1ind))

            elif are_verts_old[0] != are_verts_old[1]:
                # one vertex is in mesh and one is in ref_mesh
                voldind = eref.entities(0)[0]
                vnewind = eref.entities(0)[1]

                if are_verts_old[1]:
                    # swap variables
                    # (second one has index below maximum vertex index)
                    vnewind = eref.entites(0)[0]
                    voldind = eref.entites(0)[1]

                # the new vertex which was added in refinement
                vnew = Vertex(ref_mesh, vnewind)
                # the old vertex which is in mesh and ref_mesh
                vold = Vertex(mesh, voldind)

                # find edge which is incident to vnew but not to vold and is
                # on boundary
                vold2ind = -1
                for e in edges(vnew):
                    if edge_on_boundary(e) and (e.index() != eref.index()):
                        # e is incident to vertex with index vnewind but not
                        # with vertex with index voldind
                        # e is on the boundary
                        if e.entities(0)[0] != vnewind:
                            assert e.entities(0)[0] != voldind and e.entities(0)[0] <= maxvertind
                            vold2ind = e.entities(0)[0]
                        elif(e.entities(0)[1] != vnewind):
                            assert e.entities(0)[1] != voldind and e.entities(0)[1] <= maxvertind
                            vold2ind = e.entities(0)[1]

                # there has to be such an edge
                assert vold2ind != -1

                # find parent edge[vold vold2]
                vold = Vertex(mesh, voldind)
                vold2 = Vertex(mesh, vold2ind)
                # get edge indices of the connecting edge, arrays should be
                # unique
                parent_edgeind = np.intersect1d(vold.entities(1),
                                                vold2.entities(1), True)
                if parent_edgeind.size == 1:
                    # set parent edge
                    boundary_parent_edge[eref] = parent_edgeind[0]
                else:
                    raise ValueError("Could not construct edge in parent mesh from vertices with indices %d, %d" % (v0ind, v1ind))

            else:
                raise ValueError("Edge is on Boundary and has two nodes which indices are higher than the maximum indices of the old mesh.")

    return boundary_parent_edge


def compute_refined_edge_numbers_function(mesh, edge_numbers, ref_mesh):
    """Computes refined edge_number MeshFunction for given ref_mesh

    Function computes a parent edge relation for edges on boundary for
    ref_mesh w.r.t to mesh (s.a. compute_boundary_parent_edge_function)
    and with help of that parent edge relation the edge_numbers MeshFunction
    is refined w.r.t. ref_mesh.

    Args:
        mesh        (Mesh):            the mesh

        edge_numbers(MeshFunction):    MeshFunction, corresponding to mesh,
                                       which returns the edge number for every
                                       edge

        ref_mesh    (Mesh):            the refined mesh of argument mesh

    Returns:
        edge_numbers_ref (MeshFunction): refined edge_numbers MeshFunction
                                          w.r.t. ref_mesh

    """

    # get parent edge relation for boundary edges
    boundary_parent_edge = compute_boundary_parent_edge_function(mesh,
                                                                 ref_mesh)

    edge_numbers_ref = MeshFunction('size_t', ref_mesh, 1, CONST.EDGE_INNER)

    for e in edges(ref_mesh):
        if boundary_parent_edge[e] != -1:
            # edge is not on boundary
            assert edge_on_boundary(e)
            # parent edge is not an inner edge
            # print edge_numbers[boundary_parent_edge[e]]
            edge_numbers_ref[e] = edge_numbers[boundary_parent_edge[e]]

    return edge_numbers_ref


def edge_numbers_to_gamma_numbers(mesh, edge_numbers):
    """Takes MeshFunction edge_numbers with corresponding mesh as argument and
       computes gamma boundaries ref. Abb 1.1 and p.50.

    Function iterates over all edges of the mesh and transform edge_numbers to
    gamma_numbers Function w.r.t. defined Values in constants.py

    Args:
        mesh        (Mesh):           Mesh
        edge_numbers(MeshFunction):   To the Mesh corresponding MeshFunction

    Returns:
        gamma_numbers (MeshFunction): gamma_numbers MeshFunction defined on the
                                      edges of mesh return for every edge the
                                      gamma number (s.a.) constants.py

    Raises:
        ValueError: If value of edge_numbers[edge] is not defined in
                    CONST_EDGES_GAMMA*, then the algorithm could not assign
                    the edge_number to the corresponding gamma boundary part.
    """

    # create a new MeshFunction defined on the edges and set correct values
    gamma_numbers = MeshFunction('size_t', mesh, edge_numbers.dim(),
                                 CONST.INDEX_GAMMA_INNER)

    for edge in edges(mesh):
        # gamma 0
        if edge_numbers[edge] in CONST.EDGES_GAMMA0:
            gamma_numbers[edge] = CONST.INDEX_GAMMA0
        # gamma 1
        elif edge_numbers[edge] in CONST.EDGES_GAMMA1:
            gamma_numbers[edge] = CONST.INDEX_GAMMA1
        # gamma 2
        elif edge_numbers[edge] in CONST.EDGES_GAMMA2:
            gamma_numbers[edge] = CONST.INDEX_GAMMA2
        # gamma 3
        elif edge_numbers[edge] in CONST.EDGES_GAMMA3:
            gamma_numbers[edge] = CONST.INDEX_GAMMA3
        # gamma 4
        elif edge_numbers[edge] in CONST.EDGES_GAMMA4:
            gamma_numbers[edge] = CONST.INDEX_GAMMA4
        # gamma 5
        elif edge_numbers[edge] in CONST.EDGES_GAMMA5:
            gamma_numbers[edge] = CONST.INDEX_GAMMA5
        # gamma 6
        elif edge_numbers[edge] in CONST.EDGES_GAMMA6:
            gamma_numbers[edge] = CONST.INDEX_GAMMA6
        # gamma 7
        elif edge_numbers[edge] in CONST.EDGES_GAMMA7:
            gamma_numbers[edge] = CONST.INDEX_GAMMA7
        # inner edge default set to CONS_INDEX_GAMM_INNER
        elif not edge_numbers[edge] in CONST.EDGES_GAMMA_INNER:
            raise ValueError("Constant for CONST_EDGES_GAMMA*: %d not defined!"
                             % (edge_numbers[edge]))

    return gamma_numbers


def Ball_project(Index_Ball, mesh, edge_numbers):
    """Projection for curved boundaries ref. Abb 1.1, p.50, p.81

    Function choose for the given Ball index corresponding Radius rad and
    Centre m. Constructs an anonymus function, which returns True if Edge
    belongs to the Ball otherwise false. Iterating over each edge in mesh and
    project vertices if edge belongs to Ball.

    Args:
        Index_Ball  (int):          Index number of the Ball to project the
                                    corresponding boundary vertices.

        mesh        (Mesh):         Mesh for projection of the boundary
                                    vertices.

        edge_numbers(MeshFunction): Should correct index the edges from 1 to 20
                                    on the boundary and 0 for inner edges.

    Returns:
        Mesh: The mesh with projected boundary vertices of ball with index
              Index_Ball

    Raises:
        ValueError: If Index_Ball could not be matched to any defined
                    INDEX_BALL* constants.

    """

    if Index_Ball == CONST.INDEX_BALL1:
        rad = CONST.RAD_BALL1
        m = CONST.M_BALL1
        check_edge_number = lambda edge: edge_numbers[edge] in CONST.EDGES_BALL1
    elif Index_Ball == CONST.INDEX_BALL2:
        rad = CONST.RAD_BALL2
        m = CONST.M_BALL2
        check_edge_number = lambda edge: edge_numbers[edge] in CONST.EDGES_BALL2
    elif Index_Ball == CONST.INDEX_BALL3:
        rad = CONST.RAD_BALL3
        m = CONST.M_BALL3
        check_edge_number = lambda edge: edge_numbers[edge] in CONST.EDGES_BALL3
    elif Index_Ball == CONST.INDEX_BALL4:
        rad = CONST.RAD_BALL4
        m = CONST.M_BALL4
        check_edge_number = lambda edge: edge_numbers[edge] in CONST.EDGES_BALL4
    else:
        raise ValueError('Index_Ball: %s not defined in constants.py'
                         % (Index_Ball))

    # iterate over the edges of mesh, check edge is on correct boundary and
    # the project every node on the edge to the curved boundary
    for edge in edges(mesh):
        if check_edge_number(edge):
            for vert in vertices(edge):
                x0 = mesh.coordinates()[vert.index(), 0]
                x1 = mesh.coordinates()[vert.index(), 1]
                norm = np.sqrt((x0 - m[0])**2 + (x1 - m[1])**2)
                mesh.coordinates()[vert.index(), 0] = m[0] + (rad / norm) * \
                                                             (x0 - m[0])
                mesh.coordinates()[vert.index(), 1] = m[1] + (rad / norm) * \
                                                             (x1 - m[1])
    return mesh


def rail_refine(mesh, edge_numbers):
    """Compute refined mesh with projected boundaries, MeshFunction which
       returns edge numbers and MeshFunction which returns gamma numbers.

    Args:
        mesh            (Mesh):             mesh of rail which should be
                                            refined
        edge_numbers    (MeshFunction):     edge_numbers return the edge
                                            number of an edge of mesh

    Returns:
        ref_mesh            (Mesh)        : refined mesh with projected
                                            boundary vertices
        ref_edge_numbers    (MeshFunction): MeshFunction defined on the edges
                                            of ref_mesh which returns the edge
                                            number for each edge
        ref_gamma_numbers   (MeshFunction): MeshFunction defined on the edges
                                            of ref_mesh which returns the gamma
                                            boundary number for each edge

    Raises:
        ValueError: If CONST.REFINEMENT_ALGORITHM is not set to regular_cut or
                    recursive_bisection, there raises an ValueError because
                    Function for rail refinement are only tested with these
                    two refinement algorithms.

    """

    # set correct refinement algorithm
    parameters['refinement_algorithm'] = CONST.REFINEMENT_ALGORITHM

    if parameters['refinement_algorithm'] == 'recursive_bisection':
        dummy_marker = MeshFunction('bool', mesh, mesh.topology().dim(), True)
        ref_mesh = refine(mesh, dummy_marker)
    else:
        ref_mesh = refine(mesh)

    # compute MeshFunction which returns edge numbers for every edge
    ref_edge_numbers = compute_refined_edge_numbers_function(mesh,
                                                             edge_numbers,
                                                             ref_mesh)

    # compute MeshFunction which returns gamma boundary numbers for every edge
    ref_gamma_numbers = edge_numbers_to_gamma_numbers(ref_mesh,
                                                      ref_edge_numbers)

    # project boundaries
    Ball_project(CONST.INDEX_BALL1, ref_mesh, ref_edge_numbers)
    Ball_project(CONST.INDEX_BALL2, ref_mesh, ref_edge_numbers)
    Ball_project(CONST.INDEX_BALL3, ref_mesh, ref_edge_numbers)
    Ball_project(CONST.INDEX_BALL4, ref_mesh, ref_edge_numbers)

    return ref_mesh, ref_edge_numbers, ref_gamma_numbers


def refine_n_times(n):
    """Refine n times the macro Mesh and stores refined Meshes and MeshFunction
       with vtk files in directories defined in constants.py

    Args:
        n           (n):    number of refinments

    Returns:

    """

    # read the mesh
    mesh = Mesh(CONST.MACRO_MESH_FILE)
    # read edge numbers MeshFunction from file and compute gamma numbers
    # MeshFunction
    edge_numbers = MeshFunction('size_t', mesh, CONST.MACRO_EDGE_NUMBERS_FILE)
    gamma_numbers = edge_numbers_to_gamma_numbers(mesh, edge_numbers)

    # store files for paraview
    File(CONST.MACRO_MESH_VTK_FILE, 'compressed') << mesh
    File(CONST.MACRO_GAMMA_NUMBERS_VTK_FILE, 'compressed') << gamma_numbers
    File(CONST.MACRO_EDGE_NUMBERS_VTK_FILE, 'compressed') << edge_numbers

    # refine and store
    for i in range(1, n+1):
        print('refinement: ', i)
        # refine and get new mesh refined edge numbers function and refined
        # gamma numbers function
        ref_mesh, ref_edge_numbers, ref_gamma_numbers = rail_refine(mesh, edge_numbers)

        # save data to xml files
        File(CONST.REFN_MESH_FILE(i)) << ref_mesh
        File(CONST.REFN_EDGE_NUMBERS_FILE(i)) << ref_edge_numbers
        File(CONST.REFN_GAMMA_NUMBERS_FILE(i)) << ref_gamma_numbers

        # save data to pvd files for paraview
        File(CONST.REFN_MESH_VTK_FILE(i), 'compressed') << ref_mesh
        File(CONST.REFN_EDGE_NUMBERS_VTK_FILE(i), 'compressed') << ref_edge_numbers
        File(CONST.REFN_GAMMA_NUMBERS_VTK_FILE(i),'compressed') << ref_gamma_numbers

        # set ref_mesh to 'old' mesh
        mesh = ref_mesh
        edge_numbers = ref_edge_numbers


if __name__ == "__main__":

    # refine the macro mesh n times and store the data
    CONST.REFINEMENT_ALGORITHM = 'regular_cut'
    refine_n_times(8)
    CONST.REFINEMENT_ALGORITHM = 'bisection'
    refine_n_times(5)
    CONST.REFINEMENT_ALGORITHM = 'recursive_bisection'
    refine_n_times(5)
    CONST.REFINEMENT_ALGORITHM = 'iterative_bisection'
    refine_n_times(5)































