"""
Copyright (c) 2018-2020, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

# -*- coding: utf-8 -*-
from functools import reduce
from numpy import eye
from scipy.sparse import lil_matrix, csr_matrix
import dolfin as df

"""dolfin version"""
DOLFIN_VERSION = df.__version__

"""turn off FENiCS Output"""
df.set_log_active(False)

"""set logging options for raillogger (raillog.py), raillogevel should be
   higher than 50 otherwise FENiCS Output is also in log files"""
RAILLOG_datefmt = '%d.%m.%Y %H:%M:%S'
RAILLOG_fmt = '%(asctime)s - %(message)s'
RAILLOG_level = 60

"""by changning the backend you have to adapt the code to get access to matrix
 data"""


def get_sparsedata(M):
    """Function returns a scipy CSR Matrix for given Matrix (dolfin) M"""
    rows = M.size(0)
    cols = M.size(1)
    return csr_matrix(df.as_backend_type(M).mat().getValuesCSR()[::-1],
                      (rows, cols))


"""indices of the gamma boundary parts"""
INDEX_GAMMA0 = 0
INDEX_GAMMA1 = 1
INDEX_GAMMA2 = 2
INDEX_GAMMA3 = 3
INDEX_GAMMA4 = 4
INDEX_GAMMA5 = 5
INDEX_GAMMA6 = 6
INDEX_GAMMA7 = 7
INDEX_GAMMA_INNER = 8


"""indices of edge number boundary parts ref abb 1.1"""
EDGE_INNER = 0
EDGE_1 = 1
EDGE_2 = 2
EDGE_3 = 3
EDGE_4 = 4
EDGE_5 = 5
EDGE_6 = 6
EDGE_7 = 7
EDGE_8 = 8
EDGE_9 = 9
EDGE_10 = 10
EDGE_11 = 11
EDGE_12 = 12
EDGE_13 = 13
EDGE_14 = 14
EDGE_15 = 15
EDGE_16 = 16
EDGE_17 = 17
EDGE_18 = 18
EDGE_19 = 19
EDGE_20 = 20

"""constants for projection of the nodes on the curved boundary"""
INDEX_BALL1 = 1
M_BALL1 = [0.2655, 1.5198]
RAD_BALL1 = 0.08019162478295
EDGES_BALL1 = [EDGE_12, EDGE_13]

INDEX_BALL2 = 2
M_BALL2 = [0.22758, 0.35544]
RAD_BALL2 = 0.13758140667677
EDGES_BALL2 = [EDGE_4, EDGE_5]

INDEX_BALL3 = 3
M_BALL3 = [0.196796, 1.03778]
RAD_BALL3 = 0.10679592032499
EDGES_BALL3 = [EDGE_6, EDGE_7]

INDEX_BALL4 = 4
M_BALL4 = [0.3132, 1.23581]
RAD_BALL4 = 0.07005044877845
EDGES_BALL4 = [EDGE_9, EDGE_10]

"""edge numbers which are contained in gamma boundary part"""
EDGES_GAMMA0 = [EDGE_17, EDGE_18, EDGE_19, EDGE_20]
EDGES_GAMMA1 = [EDGE_12, EDGE_13, EDGE_14]
EDGES_GAMMA2 = [EDGE_11]
EDGES_GAMMA3 = [EDGE_6, EDGE_7, EDGE_8, EDGE_9, EDGE_10]
EDGES_GAMMA4 = [EDGE_15, EDGE_16]
EDGES_GAMMA5 = [EDGE_3, EDGE_4, EDGE_5]
EDGES_GAMMA6 = [EDGE_2]
EDGES_GAMMA7 = [EDGE_1]
EDGES_GAMMA_INNER = [EDGE_INNER]

"""algorithm for mesh refinement"""
REFINEMENT_ALGORITHM = 'regular_cut'

"""constant material parameters"""
CONST_RHO = float(654.0)
CONST_C = float(7620.0)
CONST_KAPPA = float(17.572614)
CONST_LAMBDA = float(26.4)
CONST_ALPHA = CONST_LAMBDA / (CONST_C * CONST_RHO)
CONST_GAMMAK = 2.64 * CONST_LAMBDA

"""linear material parameters p.19 (1.5)"""
LINEAR_RHO = lambda u: 175.6 * u + 454.4
LINEAR_C = lambda u: -455.3 * u + 7988
LINEAR_LAMBDA = lambda u: 12.7 * u + 14.6
LINEAR_ALPHA = lambda u: LINEAR_LAMBDA(u) / (LINEAR_C(u) * LINEAR_RHO(u))
LINEAR_GAMMAK = lambda u: 2.64 * LINEAR_LAMBDA(u)

"""start temperature, treshold temperature and temperature for cooling fluid"""
CONST_U0 = 1.000
CONST_U_INIT = 0.99
CONST_UEXT = 0.02

"""Function Space"""
FUNCTION_SPACE = 'Lagrange'
FUNCTION_DEGREE = 1

"""Solver choose solver which is available with you FEnicS installation """
LINEAR_SOLVER = "cg"
PRECONDITIONER = "hypre_amg"

"""TIME STEP SIZE"""
T_DELTA = float(1.0)
T_END = float(4500.0)
assert T_DELTA > 0
assert T_END > 0

"""time integration methods"""
EULER_EXPLICIT = 'eulerexplicit'
EULER_IMPLICIT = 'eulerimplicit'
CRANK_NICOLSON = 'cranknicolson'

"""step size between two info percent outputs log"""
PERCENT_STEP = 5
PERCENT_OUTPUT = 'Percent: %.2f%%\t\t Time:%.4fs\tu_max: %.4f\tu_min: %.4f'
PERCENT_COST_OUTPUT = 'Costs  : (Cu)\'Q(Cu) = %.2e\t q\'Rq = %.2e'
assert PERCENT_STEP > 0

"""time step size between two vtk dumps"""
VTK_SAMPLE_TIME = 100
VTK_FIELD_NAME = 'u'
VTK_LABEL_NAME = 'temperature'
VTK_OUTPUT = 'Write VTK Dump at time:%.2fs'
assert VTK_SAMPLE_TIME > 0

"""pycmess output on/off (0/1)"""
PYMESS_ADI_OUTPUT = 0
PYMESS_NM_OUTPUT = 0

"""input macro mesh files and mesh functions"""
MACRO_MESH_FILE = '../mesh/' + DOLFIN_VERSION + '/macro/macro_fenics.xml'
MACRO_EDGE_NUMBERS_FILE = '../mesh/' + DOLFIN_VERSION + \
                          '/macro/macro_edge_numbers_fenics.xml'
MACRO_GAMMA_NUMBERS_FILE = '../mesh/' + DOLFIN_VERSION + \
                           '/macro/macro_gamma_numbers_fenics.xml'

"""output for macro vtk files"""
MACRO_MESH_VTK_FILE = '../mesh/' + DOLFIN_VERSION + \
                      '/macro/VTK_DUMP/macro_fenics.pvd'
MACRO_EDGE_NUMBERS_VTK_FILE = '../mesh/' + DOLFIN_VERSION + \
                              '/macro/VTK_DUMP/macro_edge_numbers_fenics.pvd'
MACRO_GAMMA_NUMBERS_VTK_FILE = '../mesh/' + DOLFIN_VERSION + \
                               '/macro/VTK_DUMP/macro_gamma_numbers_fenics.pvd'

""" input/output for refined mesh files """
REFN_MESH_FILE = lambda num: '../mesh/' + DOLFIN_VERSION + '/' + \
                             REFINEMENT_ALGORITHM + '/ref' + str(num) + \
                             '/ref' + str(num) + '_mesh_fenics.xml.gz'
REFN_EDGE_NUMBERS_FILE = lambda num: '../mesh/' + DOLFIN_VERSION + '/' \
                                     + REFINEMENT_ALGORITHM + '/ref' + \
                                     str(num) + '/ref' + str(num) + \
                                     '_edge_numbers_fenics.xml.gz'
REFN_GAMMA_NUMBERS_FILE = lambda num: '../mesh/' + DOLFIN_VERSION + '/' + \
                                      REFINEMENT_ALGORITHM + '/ref' + str(num)\
                                      + '/ref' + str(num) + \
                                      '_gamma_numbers_fenics.xml.gz'

REFN_EDGE_NUMBERS_VTK_FILE = lambda num: '../mesh/' + DOLFIN_VERSION + '/' + \
                                         REFINEMENT_ALGORITHM + '/ref' + \
                                         str(num) + \
                                         '/VTK_DUMP/return ef' + str(num) + \
                                         '_edge_numbers_fenics.pvd'
REFN_GAMMA_NUMBERS_VTK_FILE = lambda num: '../mesh/' + DOLFIN_VERSION + '/' + \
                                          REFINEMENT_ALGORITHM + '/ref' + \
                                          str(num) + \
                                          '/VTK_DUMP/return ef' + str(num) + \
                                          '_gamma_numbers_fenics.pvd'
REFN_MESH_VTK_FILE = lambda num: '../mesh/' + DOLFIN_VERSION + '/' + \
                                 REFINEMENT_ALGORITHM + '/ref' + str(num) + \
                                 '/VTK_DUMP/ref' + str(num) + \
                                 '_mesh_fenics.pvd'

"""Output folder for ODE System Matrices of ODE_assemble in ODE_assemble.py"""
ODE_ASSEMBLE_FOLDER = '../ODE_assemble/' + DOLFIN_VERSION + '/' + \
                      REFINEMENT_ALGORITHM + '/'
ODE_ASSEMBLE_MAT_FILENAME = ODE_ASSEMBLE_FOLDER + 'ODE_%s.mat'
ODE_ASSEMBLE_MIXED_MAT_FILENAME = ODE_ASSEMBLE_FOLDER + 'ODE_mixed_%s.mat'

"""Output folder for results of ODE_ansatz_linear in
   wip_refernce_computation.py"""
ODE_ANSATZ_LINEAR_FOLDER = '../Precomputation/ODE_ansatz_linear/' + \
                           DOLFIN_VERSION + '/' + \
                           REFINEMENT_ALGORITHM + '/%s/'
ODE_ANSATZ_LINEAR_MAT_FILENAME = ODE_ANSATZ_LINEAR_FOLDER + 'u_end_data.mat'
ODE_ANSATZ_LINEAR_VTK_FILENAME = ODE_ANSATZ_LINEAR_FOLDER + 'VTK_DUMP/u.pvd'
ODE_ANSATZ_LINEAR_LOG = ODE_ANSATZ_LINEAR_FOLDER + 'raillog.txt'

"""Output folder for results of ODE_ansatz_nonlinear in
   wip_refernce_computation.py"""
ODE_ANSATZ_NONLINEAR_FOLDER = '../Precomputation/ODE_ansatz_nonlinear/' + \
                              DOLFIN_VERSION + '/' + \
                              REFINEMENT_ALGORITHM + '/%s/'
ODE_ANSATZ_NONLINEAR_MAT_FILENAME = ODE_ANSATZ_NONLINEAR_FOLDER + \
    'u_end_data.mat'
ODE_ANSATZ_NONLINEAR_VTK_FILENAME = ODE_ANSATZ_NONLINEAR_FOLDER + \
    'VTK_DUMP/u.pvd'
ODE_ANSATZ_NONLINEAR_LOG = ODE_ANSATZ_NONLINEAR_FOLDER + \
    'raillog.txt'

"""Output folder for results of PDE_ansatz_linear in
   wip_refernce_computation.py"""
PDE_ANSATZ_LINEAR_FOLDER = '../Precomputation/PDE_ansatz_linear/' + \
                           DOLFIN_VERSION + '/' + \
                           REFINEMENT_ALGORITHM + '/%s/'
PDE_ANSATZ_LINEAR_MAT_FILENAME = PDE_ANSATZ_LINEAR_FOLDER + 'u_end.mat'
PDE_ANSATZ_LINEAR_VTK_FILENAME = PDE_ANSATZ_LINEAR_FOLDER + 'VTK_DUMP/u.pvd'
PDE_ANSATZ_LINEAR_LOG = PDE_ANSATZ_LINEAR_FOLDER + 'raillog.txt'

"""Output folder for results of PDE_ansatz_nonlinear in
wip_refernce_computation.py"""
PDE_ANSATZ_NONLINEAR_FOLDER = '../Precomputation/PDE_ansatz_nonlinear/' + \
                              DOLFIN_VERSION + '/' + \
                              REFINEMENT_ALGORITHM + '/%s/'
PDE_ANSATZ_NONLINEAR_MAT_FILENAME = PDE_ANSATZ_NONLINEAR_FOLDER + 'u_end.mat'
PDE_ANSATZ_NONLINEAR_VTK_FILENAME = PDE_ANSATZ_NONLINEAR_FOLDER + \
    'VTK_DUMP/u.pvd'
PDE_ANSATZ_NONLINEAR_LOG = PDE_ANSATZ_NONLINEAR_FOLDER + 'raillog.txt'


"""Output folder/files for results of LQR_MIXED"""
# LQR Problem with mixed boundary condition  p.18 equation (1.4), constant
# coefficents
LQR_MIXED_LINEAR_FOLDER = '../LQR/LQR_MIXED_linear/' + DOLFIN_VERSION + \
                          '/' + REFINEMENT_ALGORITHM + '/%s/'
LQR_MIXED_LINEAR_MAT_FILENAME = LQR_MIXED_LINEAR_FOLDER + 'u_end.mat'
LQR_MIXED_LINEAR_VTK_FILENAME = LQR_MIXED_LINEAR_FOLDER + 'VTK_DUMP/u.pvd'
LQR_MIXED_LINEAR_LOG = LQR_MIXED_LINEAR_FOLDER + 'raillog.txt'
LQR_MIXED_LINEAR_CONTROL_LOG = LQR_MIXED_LINEAR_FOLDER + 'control.txt'
LQR_MIXED_LINEAR_OUTPUT_LOG = LQR_MIXED_LINEAR_FOLDER + 'output.txt'
# header for LQR_MIXED_LINEAR_CONTROL_LOG / LQR_MIXED_LINEAR_OUTPUT_LOG
LQR_MIXED_LINEAR_CONTROL_LOG_HEADER = 'q\n'
LQR_MIXED_LINEAR_OUTPUT_LOG_HEADER = 'y\n'
# given a list array of controls q  / output return string for writing in
# one line
LQR_MIXED_LINEAR_CONTROL_LOG_FMT = lambda q: reduce(lambda n, m: ('%.4e\t' % m)
                                                    + n, q, '') + '\n'
LQR_MIXED_LINEAR_OUTPUT_LOG_FMT = lambda y: reduce(lambda n, m: ('%.4e\t' % m)
                                                   + n, y, '') + '\n'

"""Output folder/files for results of LQR_ALT"""
# LQR Problem with u_ext as control  constant coefficents
LQR_ALT_LINEAR_FOLDER = '../LQR/LQR_ALT_linear/' + DOLFIN_VERSION + \
                        '/' + REFINEMENT_ALGORITHM + '/%s/'
LQR_ALT_LINEAR_MAT_FILENAME = LQR_ALT_LINEAR_FOLDER + 'u_end.mat'
LQR_ALT_LINEAR_VTK_FILENAME = LQR_ALT_LINEAR_FOLDER + 'VTK_DUMP/u.pvd'
LQR_ALT_LINEAR_LOG = LQR_ALT_LINEAR_FOLDER + 'raillog.txt'
LQR_ALT_LINEAR_CONTROL_LOG = LQR_ALT_LINEAR_FOLDER + 'control.txt'
LQR_ALT_LINEAR_OUTPUT_LOG = LQR_ALT_LINEAR_FOLDER + 'output.txt'
# header for LQR_ALT_LINEAR_CONTROL_LOG / LQR_ALT_LINEAR_OUTPUT_LOG
LQR_ALT_LINEAR_CONTROL_LOG_HEADER = 'u_ext_control\n'
LQR_ALT_LINEAR_OUTPUT_LOG_HEADER = 'y\n'
# give a list of controls u_ext_control / outputs return string for writing
# in one line
LQR_ALT_LINEAR_CONTROL_LOG_FMT = lambda u_ext_control: reduce(lambda n, m:
                                                              ('%.4e\t' % m) +
                                                              n, u_ext_control,
                                                              '') + '\n'
LQR_ALT_LINEAR_OUTPUT_LOG_FMT = lambda y: reduce(lambda n, m: ('%.4e\t' % m)
                                                 + n, y, '') + '\n'

"""Size of output vector y"""
LQR_CONST_Y_SIZE = 6

"""Size of controll vector q"""
LQR_CONST_q_SIZE = 7


class LQR_COST():
    '''
    linear quadratic cost printer
    '''
    def __init__(self, wR=1.0, wQ=1.0):
        self.R = wR * eye(LQR_CONST_q_SIZE)
        self.Q = wQ * eye(LQR_CONST_Y_SIZE)
        self.name = "R_%.2eI__Q_%.2eI" % (wR, wQ)


def LQR_C_60(n, vertex_to_dof):
    """
    Cost matrix ref. p. 49 /p.60, Vertex to Dof Map is needed because of dof
    reordering
    """

    if n <= 92:
        raise ValueError('n is to small. Cannot define C.')

    C = lil_matrix((LQR_CONST_Y_SIZE, n))
    C[0, vertex_to_dof[59]] = 3
    C[0, vertex_to_dof[21]] = -1
    C[0, vertex_to_dof[3]] = -1
    C[1, vertex_to_dof[62]] = 2
    C[1, vertex_to_dof[2]] = -1
    C[1, vertex_to_dof[1]] = -1
    C[2, vertex_to_dof[50]] = 1
    C[2, vertex_to_dof[42]] = -1
    C[3, vertex_to_dof[54]] = 1
    C[3, vertex_to_dof[46]] = -1
    C[4, vertex_to_dof[91]] = 2
    C[4, vertex_to_dof[8]] = -1
    C[4, vertex_to_dof[15]] = -1
    C[5, vertex_to_dof[82]] = 3
    C[5, vertex_to_dof[33]] = -1
    C[5, vertex_to_dof[9]] = -1
    C[5, vertex_to_dof[14]] = -1
    return C.todense()


def LQR_C_DIFF(n, vertex_to_dof):
    """
    Cost matrix ref. p. 49 /p.60, , Vertex to Dof Map is needed because of
    dof reordering
    """

    if n <= 92:
        raise ValueError('n is to small. Cannot define C.')

    C = lil_matrix((LQR_CONST_Y_SIZE, n))
    C[0, vertex_to_dof[59]] = 2
    C[0, vertex_to_dof[21]] = -1
    C[0, vertex_to_dof[3]] = -1
    C[1, vertex_to_dof[62]] = 2
    C[1, vertex_to_dof[2]] = -1
    C[1, vertex_to_dof[1]] = -1
    C[2, vertex_to_dof[50]] = 1
    C[2, vertex_to_dof[42]] = -1
    C[3, vertex_to_dof[54]] = 1
    C[3, vertex_to_dof[46]] = -1
    C[4, vertex_to_dof[91]] = 2
    C[4, vertex_to_dof[8]] = -1
    C[4, vertex_to_dof[15]] = -1
    C[5, vertex_to_dof[82]] = 3
    C[5, vertex_to_dof[33]] = -1
    C[5, vertex_to_dof[9]] = -1
    C[5, vertex_to_dof[14]] = -1
    return C.todense()


def LQR_C_TEMP(n, vertex_to_dof):
    """
    Cost matrix ref. p.61, Vertex to Dof Map is needed because of
    dof reordering
    """

    if n <= 92:
        raise ValueError('n is to small. Cannot define C.')

    C = lil_matrix((LQR_CONST_Y_SIZE, n))
    C[0, vertex_to_dof[59]] = 1
    C[1, vertex_to_dof[62]] = 1
    C[2, vertex_to_dof[50]] = 1
    C[3, vertex_to_dof[54]] = 1
    C[4, vertex_to_dof[91]] = 1
    C[5, vertex_to_dof[82]] = 1
    return C.todense()
