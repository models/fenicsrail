#!/bin/sh

#script looks in the following directories and renders videos
DIR=['../LQR' '../Precomputation']
#DIR=['../LQR/LQR_MIXED_linear']
#DIR=['../Precomputation/']



for dir in ${DIR[@]};
	do
	for file in $(find $dir -name *.pvd);
		do
		#delete pvd and add avi
		avifile=${file/.pvd/.avi};

		#construct avifile and title
		avifile=${avifile/VTK_DUMP\/}; 		#delete VTK_DUMP
		#echo $avifile
		title=${avifile%/u.avi};			#delete u.avi
		#title=${title/[0-9].[0-9].[0-9]\/}; #delete version number
		title=${title/..\/};				#delete ..
		#title=${title//\/};					#remove /
		#echo $title

		#render video with render.py skript
		echo render $file

		#sets title
		pvpython render.py $file $avifile $title

		#sets no title
		#pvpython render.py $file $avifile

	done

done
