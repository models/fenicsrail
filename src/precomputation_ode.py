"""
Copyright (c) 2018-2020, Maximilian Behr, Jens Saak
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FEniCSRail project.
"""

from scipy.io import savemat
from dolfin import (Constant, Measure, Mesh, MeshFunction, Function,
                    TrialFunction, TestFunction, inner, grad, assemble, dx,
                    FunctionSpace, project, solve, File,
                    vertex_to_dof_map, dof_to_vertex_map)
import numpy as np
import constants as CONST
import raillog


def ODE_ansatz_linear(mesh_file, gamma_numbers_file, integrator):
    """Precomputation (4.2 Die ungesteuerte Referenzrechnung) p.47. with
       ODE-Ansatz.
       Boundary condition p.18 (1.3) with q_k == kappa.
       Use constants coefficients rho, c, lambda.

    Function takes the constants from wip_constants, define a weak formulation
    and solve it with ODE-Ansatz and numerical integration.
    Save temperature in vtk file and temperature at the end in mat file,
    s.a. constants.py

     Args:
        mesh_file           (String): points to the .xml mesh file
                                      (mesh folder)
        gamma_numbers_file  (String): points to the .xml MeshFunction file
                                      (mesh folder)
        integrator          (String): CONST.EULER_IMPLICIT,
                                      CONST.EULER_EXPLICIT,
                                      CONST.CRANK_NICOLSON

    Returns:

    Raises: ValueError if integrator is not CONST.EULER_IMPLICIT,
                                            CONST.EULER_EXPLICIT,
                                            CONST.CRANK_NICOLSON

    Information:

    """

    # set constants
    const_rho = Constant(CONST.CONST_RHO)
    const_c = Constant(CONST.CONST_C)
    const_kappa = Constant(CONST.CONST_KAPPA)
    const_alpha = Constant(CONST.CONST_ALPHA)
    const_u_ext = Constant(CONST.CONST_UEXT)

    # read mesh and gamma_numbers
    mesh = Mesh(mesh_file)
    gamma_numbers = MeshFunction('size_t', mesh, gamma_numbers_file)

    # define function space linear Lagrange P1
    V = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    # set time step size and simulation time and t for iterating over time
    delta_t = CONST.T_DELTA
    T = CONST.T_END
    t = 0

    # the solution u and the derivative du_dt
    u = TrialFunction(V)
    du_dt = TrialFunction(V)
    v = TestFunction(V)

    # define terms of weak formulations
    ds = Measure('ds')
    ds = ds(subdomain_data=gamma_numbers)
    M_var = du_dt * v * dx
    S_var = const_alpha * inner(grad(u), grad(v)) * dx
    M_GAMMA_var = sum([const_kappa / (const_c * const_rho) * u * v * ds(i)
                       for i in range(1, 8)])
    B_var = sum([const_kappa / (const_c * const_rho) * const_u_ext * v * ds(i)
                 for i in range(1, 8)])

    # assemble matrices
    M = assemble(M_var)
    S = assemble(S_var)
    M_GAMMA = assemble(M_GAMMA_var)
    B = assemble(B_var)
    delta_tB = delta_t * B

    # define u0 as constant temperature
    u_0 = Constant(CONST.CONST_U0)

    # project u_0 down to  function space
    u_k_1 = project(u_0, V)

    # the solution
    u = Function(V)

    # M_iter*u^{k} = A_iter * u{k-1} + delta_t * B
    if integrator == CONST.EULER_IMPLICIT:
        M_iter = M + delta_t * (S + M_GAMMA)
        A_iter = M
    elif integrator == CONST.EULER_EXPLICIT:
        M_iter = M
        A_iter = M - delta_t * (S + M_GAMMA)
    elif integrator == CONST.CRANK_NICOLSON:
        M_iter = M + delta_t * 0.5 * (S + M_GAMMA)
        A_iter = M - delta_t * 0.5 * (S + M_GAMMA)
    else:
        raise ValueError('integration method: %s is not implemented' %
                         (integrator))

    # set name in vtk file
    u_k_1.rename(CONST.VTK_FIELD_NAME, CONST.VTK_LABEL_NAME)

    # file for visualization
    vtk_file = File(CONST.ODE_ANSATZ_LINEAR_VTK_FILENAME %
                    (str(mesh.num_vertices()) + '_' + integrator),
                    'compressed')
    vtk_file << u_k_1

    # add file handler to raillogger
    raillog.add_file_handler(CONST.ODE_ANSATZ_LINEAR_LOG %
                             (str(mesh.num_vertices()) + '_' + integrator))
    raillog.log(__file__ + "-ODE_ansatz_linear")
    raillog.log("mesh_file=%s" % mesh_file)
    raillog.log("gamma_numbers_file=%s" % gamma_numbers_file)
    raillog.log("integrator=%s" % integrator)

    # debug info
    old_percent = -CONST.PERCENT_STEP

    # time between two vtk sample
    old_time = -CONST.VTK_SAMPLE_TIME

    # start simulation
    while t <= T:
        # compute rhs and solve equation system
        b = A_iter * u_k_1.vector() + delta_tB
        solve(M_iter, u.vector(), b, CONST.LINEAR_SOLVER)

        # write vtk dump if time difference is large enough
        if (t - old_time) >= CONST.VTK_SAMPLE_TIME:
            raillog.log(CONST.VTK_OUTPUT % (t / 100))
            vtk_file << u_k_1
            old_time = t

        # print debug information if percent difference is large enough
        percent = (t / T) * 100
        if (percent - old_percent) >= CONST.PERCENT_STEP:
            raillog.log(CONST.PERCENT_OUTPUT % (percent, t / 100,
                                                u.vector().max(),
                                                u.vector().min()))
            old_percent = percent

        # increment time and u_k_1<-u_k
        t += delta_t
        u_k_1.assign(u)
    # end simulation

    raillog.log(CONST.PERCENT_OUTPUT % (percent, t / 100,
                                        u.vector().max(),
                                        u.vector().min()))

    # save result
    u_file = CONST.ODE_ANSATZ_LINEAR_MAT_FILENAME % (str(mesh.num_vertices()) +
                                                     '_' + integrator)

    M_csr = CONST.get_sparsedata(M)
    S_csr = CONST.get_sparsedata(S)
    M_GAMMA_csr = CONST.get_sparsedata(M_GAMMA)

    savemat(u_file,
            mdict={'u_end': np.array(u.vector()),
                   'T': T,
                   'delta_t': delta_t,
                   'integrator': integrator,
                   'doftovertexmap': dof_to_vertex_map(V),
                   'vertextodofmap': vertex_to_dof_map(V),
                   'M': M_csr,
                   'S': S_csr,
                   'M_GAMMA': M_GAMMA_csr,
                   'B': np.array(B)})

    raillog.log('Save data in %s' % u_file)

    # remove file_handler from railloger
    raillog.remove_file_handlers_handlers()


def ODE_ansatz_nonlinear(mesh_file, gamma_numbers_file, integrator):
    """Precomputation (4.2 Die ungesteuerte Referenzrechnung)
       p.47. with ODE-Ansatz.
       Boundary condition p.18 (1.3) with q_k == kappa.
       Use linear coefficients rho, c, lambda.

       Function takes the constants from wip_constants, define a weak
       formulation and solve it with ODE-Ansatz and numerical integration.
       Save temperature in vtk file and temperature at the end in mat file see
       constants.py

     Args:
        mesh_file           (String): points to the .xml mesh file
                                      (mesh folder)
        gamma_numbers_file  (String): points to the .xml MeshFunction file
                                      (mesh folder)
        integrator          (String): CONST.EULER_IMPLICIT,
                                      CONST.EULER_EXPLICIT,
                                      CONST.CRANK_NICOLSON

    Returns:

    Raises: ValueError If integrator is not CONST.EULER_IMPLICIT,
                                            CONST.EULER_EXPLICIT,
                                            CONST.CRANK_NICOLSON

    Information:

    """

    # set constants or linear functions
    rho = CONST.LINEAR_RHO
    c = CONST.LINEAR_C
    const_kappa = Constant(CONST.CONST_KAPPA)
    alpha = CONST.LINEAR_ALPHA
    const_u_ext = Constant(CONST.CONST_UEXT)

    # read mesh and gamma_numbers
    mesh = Mesh(mesh_file)
    gamma_numbers = MeshFunction('size_t', mesh, gamma_numbers_file)

    # define function space linear Lagrange P1
    V = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    # set time step size and simulation time and t for iterating over time
    delta_t = CONST.T_DELTA
    T = CONST.T_END
    t = 0

    # the solution u and the derivative du_dt
    u = TrialFunction(V)
    du_dt = TrialFunction(V)
    v = TestFunction(V)

    # define u0 as constant temperature
    u_0 = Constant(CONST.CONST_U0)

    # project u_0 down to  function space
    u_k_1 = project(u_0, V)

    # define terms of weak formulations
    ds = Measure('ds')[gamma_numbers]
    M_var = du_dt * v * dx
    S_var = alpha(u_k_1) * inner(grad(u), grad(v)) * dx
    M_GAMMA_var = sum([const_kappa / (c(u_k_1) * rho(u_k_1)) * u * v * ds(i)
                       for i in range(1, 8)])
    B_var = sum([const_kappa / (c(u_k_1) * rho(u_k_1)) * const_u_ext * v
                 * ds(i) for i in range(1, 8)])
    delta_tB_var = delta_t * B_var

    # M_iter * u^{k} = A_iter * u{k-1} + delta_t * B
    if integrator == CONST.EULER_IMPLICIT:
        M_iter_var = M_var + delta_t * (S_var + M_GAMMA_var)
        A_iter_var = M_var
    elif integrator == CONST.EULER_EXPLICIT:
        M_iter_var = M_var
        A_iter_var = M_var - delta_t * (S_var + M_GAMMA_var)
    elif integrator == CONST.CRANK_NICOLSON:
        M_iter_var = M_var + delta_t * 0.5 * (S_var + M_GAMMA_var)
        A_iter_var = M_var - delta_t * 0.5 * (S_var + M_GAMMA_var)
    else:
        raise ValueError('integration method: %s is not implemented' %
                         (integrator))

    # the solution
    u = Function(V)

    # set name in vtk file
    u_k_1.rename(CONST.VTK_FIELD_NAME, CONST.VTK_LABEL_NAME)

    # file for visualization
    vtk_file = File(CONST.ODE_ANSATZ_NONLINEAR_VTK_FILENAME %
                    (str(mesh.num_vertices()) + '_' + integrator),
                    'compressed')
    vtk_file << u_k_1

    # add file handler to raillogger
    raillog.add_file_handler(CONST.ODE_ANSATZ_NONLINEAR_LOG %
                             (str(mesh.num_vertices()) + '_' + integrator))
    raillog.log(__file__ + "-ODE_ansatz_nonlinear")
    raillog.log("mesh_file=%s" % mesh_file)
    raillog.log("gamma_numbers_file=%s" % gamma_numbers_file)
    raillog.log("integrator=%s" % integrator)

    # debug info
    old_percent = -CONST.PERCENT_STEP

    # time between two vtk sample
    old_time = -CONST.VTK_SAMPLE_TIME

    # start simulation
    while t <= T:
        # assemble matrices to get new updates in coefficients from u_k_1
        M_iter = assemble(M_iter_var)
        A_iter = assemble(A_iter_var)
        delta_tB = assemble(delta_tB_var)

        # compute rhs and solve equation system
        b = A_iter * u_k_1.vector() + delta_tB
        solve(M_iter, u.vector(), b, CONST.LINEAR_SOLVER)

        # print debug information if percent difference is large enough
        if (t - old_time) >= CONST.VTK_SAMPLE_TIME:
            raillog.log(CONST.VTK_OUTPUT % (t / 100))
            vtk_file << u_k_1
            old_time = t

        # print debug information if percent difference is large enough
        percent = (t / T) * 100
        if (percent - old_percent) >= CONST.PERCENT_STEP:
            raillog.log(CONST.PERCENT_OUTPUT % (percent, t / 100,
                                                u.vector().max(),
                                                u.vector().min()))
            old_percent = percent

        # increment time and u_k_1<-u_k
        t += delta_t
        u_k_1.assign(u)
    # end simulation

    raillog.log(CONST.PERCENT_OUTPUT % (percent, t / 100,
                                        u.vector().max(),
                                        u.vector().min()))

    # save result
    u_file = CONST.ODE_ANSATZ_NONLINEAR_MAT_FILENAME %\
        (str(mesh.num_vertices()) + '_' + integrator)

    savemat(u_file,
            mdict={'u_end': np.array(u.vector()),
                   'T': T,
                   'delta_t': delta_t,
                   'integrator': integrator,
                   'doftovertexmap': dof_to_vertex_map(V),
                   'vertextodofmap': vertex_to_dof_map(V)})

    raillog.log('Save data in %s' % u_file)

    # remove file_handler from railloger
    raillog.remove_file_handlers_handlers()


if __name__ == '__main__':
    # integrators = [CONST.EULER_IMPLICIT,
    #                CONST.EULER_IMPLICIT,
    #                CONST.CRANK_NICOLSON]
    integrators = [CONST.EULER_IMPLICIT]
    for integrator in integrators:
        for i in range(1, 3):
            print("----------------------- ref%d ------------------------" % i)
            ODE_ansatz_linear(CONST.REFN_MESH_FILE(i),
                              CONST.REFN_GAMMA_NUMBERS_FILE(i),
                              integrator)
            ODE_ansatz_nonlinear(CONST.REFN_MESH_FILE(i),
                                 CONST.REFN_GAMMA_NUMBERS_FILE(i),
                                 integrator)
