from dolfin import log, IndexSet
from dolfin.common import constants
from dolfin import mesh, MeshFunction, Cell, Edge, Vertex, MeshEditor, dolfin_error, Vector, Mesh
from dolfin.cpp.common import not_working_in_parallel, DOLFIN_EPS
import numpy as np

from dolfin import *


no_refinement               = -1
regular_refinement          = -2
backtrack_bisection         = -3
backtrack_bisection_refine  = -4

assert no_refinement                == -1
assert regular_refinement           == -2
assert backtrack_bisection          == -3
assert backtrack_bisection_refine   == -4

def refine(mesh,  cell_markers):
    #@Todo: Check type of input args


    not_working_in_parallel("RegularCutRefinement.refine")

    #only implemented in 2D
    if(mesh.topology().dim() !=2):
        dolfin_error("RegularCutRefinement.py","refine mesh","Mesh is not two-dimensional: regular-cut mesh refinement is currently only implemented in 2D")

    #check that mesh is ordered
    if not(mesh.ordered()):
        dolfin_error("RegularCutRefinement.py","refine mesh","Mesh is not ordered according to the UFC numbering convention, consider calling mesh.order()");


    # Initialize Edges
    mesh.init(1)

    # Compute refinement markers
    refinement_markers= None #@Todo:std::vector<int>
    marked_edges = IndexSet(mesh.num_edges())
    compute_markers(refinement_markers, marked_edges, mesh, cell_markers)

    # Refine mesh based on computed markers
    refined_mesh = Mesh()
    return refine_marked(refined_mesh, mesh, refinement_markers, marked_edges);

def compute_markers( refinement_markers, marked_edges, mesh, cell_markers):

    # Topological dimension
    print mesh
    D= mesh.topology().dim()

    #Create edge markers and initialize to false
    edges_per_cell = D + 1
    #edge_markers = np.array(mesh.num_cells(),dtype=np.bool)
    #for i in range(mesh.num_cells()):
    #    edge_markers[i].resize(edges_per_cell)
    #    for j in range(edges_per_cell):
    #        edge_markers[i][j]=False
    edge_markers = np.zeros((mesh.num_cells(),edges_per_cell),dtype=np.bool);


    # Create index sets for marked cells
    #cells           = IndexSet(mesh.num_cells())
    #marked_cells    = IndexSet(mesh.num_cells())
    cellsindexes        = range(mesh.num_cells())
    marked_cellsindexes = range(mesh.num_cells())

    # Get bisection data
    bisection_twins = None
    #if(mesh.data().exists('bisection_twins',D)):
    #    bisection_twins = mesh.data().array('bisection_twins',D)


    # Iterate until no more cells are marked
    #cells.fill()
    #while not (cells.empty()):
    while not ( cellsindexes):
        # Iterate over all cells in list
        #for _i in range(cells.size()):
        for _i in range(len(cellsindexes)):
            # Get cell index and create cell
            cell_index =  cellsindexes[_i]
            cell = Cell(mesh, cell_index)

            # Count the number of marked edges
            num_marked = count_markers(edge_markers[cell_index])

            # check whether cell has a bisection twin
            bisection_twin = cell_index
            is_bisected = False
            if(bisection_twins):
                bisection_twin = bisection_twins[cell_index]
                is_bisected = (bisection_twin != cell_index)

            # Get bisection edge
            common_edge = 0
            bisection_edge =0
            if(is_bisected):
                common_edge     = find_common_edges(cell,mesh,bisection_twin).first
                bisection_edge  = find_bisection_edges(cell,mesh,bisection_twin).first

            # Decide if cell should be refined
            refine = False
            # @ Todo: simplify  or take closer closer look to code
            refine = (refine or cell_markers[cell_index])
            if (is_bisected):
                refine = (refine or (num_marked > 0))
            else:
                refine = (refine or (num_marked > 1))
                refine = (refine or too_thin(cell, edge_markers[cell_index]))

            # Skip cell if it should not be marked
            if (not refine):
                continue

            # Iterate over edges
            for edge in edges(cell):
                # Skip ege if it is a bisected edge of a bisected cell
                if (is_bisected and (edge.pos() == bisection_edge)):
                    continue

                # Mark edge in current cell
                if (not edge_markers[cell_index][edge.pos()]):
                    edge_markers[cell_index][edge.pos()]=True
                    marked_cellsindexes.insert(cell_index)

                # Insert edge into set of marked edges but only if the edge
                # is not the common edge of a bisected cell in which case it
                # will later be removed and no new vertex be inserted...
                if( (not is_bisected) or (edge.pos() != common_edge)):
                    marked_edges.insert(edge.index())

                # Iterate over cells sharing edge
                for neighbor in cells(edge):
                    # Get local edge number of edge relative to neighbor
                    local_index = neighbor.index(edge)

                    # Mark edge for refinement
                    if (not (edge_markers[neighbor.index()][local_index])):
                        edge_markers[neighbor.index()][local_index]=True
                        marked_cellsindexes.insert(neighbor.index())

        #Copy marked cells
        cellsindexes = marked_cellsindexes
        #marked_cells.clear()
        del marked_cellsindexes[:]

    # Extract which cells to refine and indices which edges to bisect
    refinement_markers.resize(mesh.num_cells())
    for i in range(edge_markers.size()):
        # Count the number of marked edges
        num_marked = count_markers(edge_markers[i])

        # Check if cell has been bisected before
        is_bisected = (bisection_twins and (bisection_twins[i]!=i))

        # No refinement
        if (num_marked ==0):
            refinement_markers[i] = no_refinement
        # Mark for bisection
        elif (num_marked == 1 and (not is_bisected)):
            refinement_markers[i] = extract_edge(edge_markers[i])
        # Mark for regular refinement
        elif (num_marked == edges_per_cell and (not is_bisected)):
            refinement_markers[i] = regular_refinement
        # Mark for bisection backtracking
        elif (num_marked == 2 and is_bisected):
            refinement_markers[i] = backtrack_bisection
        # Mark for bisection backtracking and refinement
        elif (num_marked == edges_per_cell and is_bisected):
            refinement_markers[i] = backtrack_bisection_refine
        # Sanity check
        else:
            dolfin_error("RegularCutRefinement.cpp","compute marked edges","Unexpected number of edges marked")

def refine_marked( refined_mesh, mesh, refinement_markers, marked_edges):
    # Count the number of cells in refined mesh
    num_cells =0

    #Data structure to hold a cell
    cell_data = np.zeros(3,dtype=np.uint)

    for cell in cells(mesh):
        marker = refinement_markers[cell.index()]

        if marker == no_refinement:
            num_cells +=1
        elif marker == regular_refinement:
            num_cells +=4
        elif marker == backtrack_bisection:
            num_cells +=2
        elif marker == backtrack_bisection_refine:
            num_cells +=3
        else:
            num_cells+=2;



    # Initialize mesh editor
    num_vertices = mesh.num_vertices() + marked_edges.size()
    editor = MeshEditor()
    editor.open(refined_mesh, mesh.topology().dim(), mesh.geometry().dim());
    editor.init_vertices(num_vertices);
    editor.init_cells(num_cells);

    # Set vertex coordinates
    current_vertex =0
    for vertex in vertices(mesh):
        editor.add_vertex(current_vertex, vertex.point())
        current_vertex+=1

    for i in range(marked_edges.size()):
        edge = Edge(mesh,marked_edges[i])
        editor.add_vertex(current_vertex, edge.midpoint())
        current_vertex +=1


    # Get bisection data for old mesh
    D = mesh.topology().dim()
    bisection_twins=None
    #if (mesh.data().exists('bisection_twins',D)):
    #    bisection_twins = mesh.data().array('bisection_twins',D)

    # Marker for bisected cells pointing to their bisection twins in refined mesh
    #@todo pointer

    refined_bisection_twins = refined_mesh.data().create_array('bisection_twins',D)
    refined_bisection_twins.resize(num_cells)
    for i in range(num_cells):
        refined_bisection_twins[i]=i

    # Mapping from old to new unrefined cells ( -1 means refined or not yet processed)
    unrefined_cells = np.empty(mesh.num_cells(),dtype=np.int)
    unrefined_cells.fill(-1)

    # Iterate over all cells and add new cells
    current_cell = 0
    cells_array = np.zeros([4,3],dtype=np.uint)
    for cell in cells(mesh):

        # Get marker
        marker = refinement_markers[cell.index()]

        if (marker ==no_refinement):
            #No refinement: just copy cell to new mesh
            vertices_array = np.empty(0,dtype=np.uint)

            for vertex in vertices(cell):
                vertices_array = np.append(vertices_array, vertex.index())

            editor.add_cell(current_cell,vertices_array)

            # Store mapping to new cell index
            unrefined_cells[cell.index()]=current_cell

            current_cell+=1

            #Remember unrefined bisection twins
            if (bisection_twins):
                bisection_twin = bisection_twins[cell.index()]
                twin_marker = refinement_markers[bisection_twin]
                assert twin_marker == refinement_markers[bisection_twin]
                if unrefined_cells[bisection_twin]>=0:
                    i = current_cell -1
                    j = unrefined_cells[bisection_twin]
                    refined_bisection_twins[i]=j
                    refined_bisection_twins[j]=i

            elif (marker==regular_refinement):
                # Regular refinement: divide into subsimplicies
                assert  unrefined_cells[cell.index()] == -1

                # Get vertices and edges
                v = cell.entities(0)
                e = cell.entites(1)

                #@todo: assertion check against original code
                #assert len(v)>0
                #assert len(e)>0

                # Get offset for new vertex indices
                offset = mesh.num_vertices()

                # Compute indices for the six new vertices
                v0 = v[0]
                v1 = v[1]
                v2 = v[2]
                e0 = offset + marked_edges.find(e[0])
                e1 = offset + marked_edges.find(e[1])
                e2 = offset + marked_edges.find(e[2])

                # Create four new cells
                cells_array[0][0] = v0
                cells_array[0][1] = e2
                cells_array[0][2] = e1

                cells_array[1][0] = v1
                cells_array[1][1] = e0
                cells_array[1][2] = e2

                cells_array[2][0] = v2
                cells_array[2][1] = e1
                cells_array[2][2] = e0

                cells_array[3][0] = e0
                cells_array[3][1] = e1
                cells_array[3][2] = e2

                # Add cells
                for i in range(4):
                    editor.add_cell(current_cell,cells_array[i][:])
                    current_cell+=1

            elif (marker==backtrack_bisection or marker==backtrack_bisection_refine):
                #Special case. backtrack bisected cells
                assert  unrefined_cells[cell.index()]==-1

                # Get index for bisection twin
                #assert bisection_twins
                if not(bisection_twins):
                    raise AssertionError('bisection_twins assert to be not None')

                bisection_twin = bisection_twins[cell.index()]

                assert bisection_twin != cell.index()

                # Let lowest number twin handle refinement
                if (bisection_twin < cell.index()):
                    continue

                # Get marker for twin
                twin_marker = refinement_markers[bisection_twin]

                # Find common edge(s) and bisected edge(s)
                common_edges        = find_common_edges(cell, mesh, bisection_twin)
                bisection_edges     = find_bisection_edges(cell, mesh, bisection_twin)
                bisection_vertices  = find_bisection_vertices(cell, mesh, bisection_twin, bisection_edges)

                # Get list of vertices and edges for both cells
                twin        = Cell(mesh, bisection_twin)
                vertices_0  = cell.entites(0)
                vertices_1  = twin.entites(0)
                edges_0     = cell.entites(1)
                edges_1     = twin.entites(1)

                #assert vertices_0
                if (vertices_0 == None):
                    raise AssertionError('vertices_0 assert to be not None')

                #assert vertices_1
                if (vertices_1 == None):
                    raise AssertionError('vertices_1 assert to be not None')

                #assert edge_0
                if (edges_0 == None):
                    raise AssertionError('edge_0 assert to be not None')

                #assert edge_1
                if (edges_1 == None):
                    raise AssertionError('edge_1 assert to be not None')


                # Get offset for new vertex indices
                offset = mesh.num_vertices()

                # Locate vertices such that v_i is the vertex opposite to the edge e_i on the parent triangle
                v0 = vertices_0[common_edges.first]
                v1 = vertices_1[common_edges.second]
                v2 = vertices_0[bisection_edges.first]
                e0 = offset + marked_edges.find(edges_1[bisection_vertices.second])
                e1 = offset + marked_edges.find(edges_0[bisection_vertices.first])
                e2 = vertices_0[bisection_vertices.first]


                # Locate new vertices on bisected edge (if any)
                E0 = 0
                E1 = 1
                if (marker == backtrack_bisection_refine):
                    E0 = offset + marked_edges.find(edges_0[bisection_edges.first])
                if (twin_marker ==backtrack_bisection_refine):
                    E1 = offset + marked_edges.find(edges_1[bisection_edges.second])

                #Add middle two cells (always)
                assert cell_data.size() == 3
                cell_data[0] = e0
                cell_data[1] = e1
                cell_data[2] = e2
                editor.add_cell(current_cell, cell_data)
                current_cell +=1

                cell_data[0] = v2
                cell_data[1] = e1
                cell_data[2] = e0
                editor.add_cell(current_cell, cell_data)
                current_cell +=1

                # Add one or two remaining cells in current cell (left)
                if (marker == backtrack_bisection):
                    cell_data[0]=v0
                    cell_data[1]=e2
                    cell_data[2]=e1
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1
                else:
                    # Add the two cells
                    cell_data[0] = v0
                    cell_data[1] = E0
                    cell_data[2] = e1
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1

                    cell_data[0] = E0
                    cell_data[1] = e2
                    cell_data[2] = e1
                    editor.add_cell(current_cell,cell_data)
                    current_cell +=1

                    # Set bisection twins
                    refined_bisection_twins[current_cell -2]= current_cell -1
                    refined_bisection_twins[current_cell -1]= current_cell -2

            else:
                #One edge marked for refinement: do bisection
                assert unrefined_cells[cell.index()]==-1

                #Get vertices and edges
                v = cell.entites(0)
                e = cell.entites(1)
                #@Todo: check this assert
                #assert v
                #assert e

                # Get edge number (equal to marker)
                assert marker >=0
                local_edge_index    = marker
                global_edge_index   = e[local_edge_index]
                ee = mesh.num_vertices() +marked_edges.find(global_edge_index)

                # Add the two new cells
                if (local_edge_index == 0):
                    cell_data[0] = v[0]
                    cell_data[1] = ee
                    cell_data[2] = v[1]
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1

                    cell_data[0] = v[0]
                    cell_data[1] = ee
                    cell_data[2] = v[2]
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1
                elif(local_edge_index ==1):
                    cell_data[0] = v[1]
                    cell_data[1] = ee
                    cell_data[2] = v[0]
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1

                    cell_data[0] = v[1]
                    cell_data[1] = ee
                    cell_data[2] = v[2]
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1
                else :
                    cell_data[0] = v[2]
                    cell_data[1] = ee
                    cell_data[2] = v[0]
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1

                    cell_data[0] = v[2]
                    cell_data[1] = ee
                    cell_data[2] = v[1]
                    editor.add_cell(current_cell,cell_data)
                    current_cell+=1

            refined_bisection_twins[current_cell -2] = current_cell -1
            refined_bisection_twins[current_cell -1] = current_cell -2

    assert num_cells == current_cell
    editor.close()

def count_markers(markers):
    num_markers =0
    for i in range(markers.size):
        if (markers[i]):
            return i;

    dolfin_error("RegularCutRefinement.py","extract edge", "Internal error in algorithm:edge not found")

    return 0

def too_thin(cell,edge_markers):
    num_markers = count_markers(edge_markers);

    #Only care about the case when on edge is marked
    if not (num_markers == 1):
        return False;

    #Compute lengths of all edges
    lengths= np.array([],dtype=np.double)
    L = 0.0
    for edge in edges(cell):
        l = edge.length
        L = max(L,l)
        lengths=np.append(lengths,l)

    #Get length of marked edge
    l = length[extract_edge(edge_markers)]

    # Check condition
    too_thin = l<0.5

    return too_thin

def find_common_edges(cell,mesh,bisection_twin):

    #Get list of edges for both cells
    twin = Cell(mesh,bisection_twin)
    e0 = cell.entities(1)
    e1 = twin.entities(1)

    for i in range(cell.num_edges):

        #Get list of vertices for edge
        edge_0 = Edge(mesh,e0[i])
        v0     = edge_0.entities(0)

        for j in range(num_edges):

            #Dont test against the edge it
            if e0[i]==e1 [j]:
                continue

            #Get list of  vertices for edge
            edge_1 = Edge(mesh, e1[j])
            v1 = edge_1.entites(0)
            assert v1

            #Check that we have a common vertex
            if ((v0[0] != v1[0]) and (v0[0] != v1[1]) and (v0[1] != v1[0]) and (v0[1] != v1[1])):
                continue;

            #Compute normalized dot product between edges
            dot_product = edge_0.dot(edge_1)
            dot_product /= edge_0.length() +edge_1.length()

            # Bisection edge found if dot prodct is small
            if abs(abs(dot_product -1) < DOLFIN_EPS_LARGE):
                return (i,j)

    dolfin_error("RegularCutRefinement.py", "find edge for bisection", "Internal error in algorithm; bisection edge not found")

    return (0,0)

def find_bisection_edges(cell,mesh,bisection_twin):

    twin    = Cell(mesh,bisection_twin)
    e0      = cell.entities(1)
    e1      = twin.entities(1)

    #iterate over all combinations of edges
    num_edges = cell.num_entities(1)
    for i in range(num_edges):
        #get list of vertices for edge
        edge_0 = Edge(mesh,e0[i])
        v0 =edge_0.entities(0)

        for j in range(num_edges):
            if(e0[i]==e1[j]):
                continue

        #Get list of vertices for edge
        edge_1  = Edge(mesh,e1[j])
        v1      = edge_1.entities(0)

        if((v0[0]!=v1[0]) and (v0[0] != v1[1]) and (v0[1]!=v1[0]) and (v0[1] != v1[1])):
            continue;

        # Compute normalized dot product beween edges
        dot_product = edge_0.dot(edge_1)
        dot_product /= edge_0.length() *edge_1.length()

        #Bisection edge found if dot product is small
        if(abs(abs(dot_product)-1)<DOLFIN_EPS_LARGE):
            return (i,j)



    dolfin_error("RegularCutRefinement.py","find edge for bisection","Internal error in algorithm; bisection edge not found")
    return (0,0)







def find_bisection_vertices(cell, mesh, bisection_twin, bisection_edges):

    #Get list of edges for both cells

    twin = Cell(mesh, bisection_twin)
    e0 = cell.entities(1)
    e1 = twin.entities(1)
    assert e0
    assert e1

    # Get vertices of two edges
    edge_0 = Edge(mesh, e0[bisection_edges.first]);
    edge_1 = Edge(mesh, e1[bisection_edges.second]);
    v0 = edge_0.entities(0);
    v1 = edge_1.entities(0);
    assert v0
    assert v1


    #Find common vertex
    if ((v0[0] == v1[0]) or (v0[0] == v1[1])):
        v =Vertex(mesh, v0[0])
        return (cell.index(v), twin.index(v))
    if ((v0[1] == v1[0]) or (v0[1] == v1[1])):
        v = Vertex (mesh, v0[1])
        return (cell.index(v), twin.index(v))


    dolfin_error("RegularCutRefinement.py",  "find bisection vertices","Internal error in algorithm: bisection vertex not found");
    return (0,0)



if __name__ =="__main__":
    pass