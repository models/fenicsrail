%% define rectangles for gamma_0 to gamma_7
close all
%function handle for generating coordinates of rectangle
myrectangle= @(x01,x02,y01,y02) [[x01;x02;x02;x01;x01],[y01;y01;y02;y02;y01]]

%constant abritrary chosen only for plotting
Dolfineps=0.0;

hold on
axis equal

Colors = [linspace(0.0,0.3,20)',linspace(0.3,0.6,20)',linspace(0.6,1,20)']

% %% define gamma rectangles
% gamma_0_rect=myrectangle(-Dolfineps+0       , 0       + Dolfineps ,-Dolfineps + 0      ,    1.6     + Dolfineps);
% gamma_1_rect=myrectangle(-Dolfineps+0       , 0.34872 + Dolfineps ,-Dolfineps + 1.50207,    1.6     + Dolfineps);
% gamma_2_rect=myrectangle(-Dolfineps+0.345   , 0.3826  + Dolfineps ,-Dolfineps + 1.245  ,    1.5303  + Dolfineps);
% gamma_3_rect=myrectangle(-Dolfineps+0.09    , 0.3826  + Dolfineps ,-Dolfineps + 1.03778,    1.245   + Dolfineps);
% gamma_4_rect=myrectangle(-Dolfineps+0.09    , 0.09    + Dolfineps ,-Dolfineps + 0.35544,    1.03778 + Dolfineps);
% gamma_5_rect=myrectangle(-Dolfineps+0.09    , 0.5     + Dolfineps ,-Dolfineps + 0.15   ,    0.35544 + Dolfineps);
% gamma_6_rect=myrectangle(-Dolfineps+0.5     , 0.5     + Dolfineps ,-Dolfineps + 0.0    ,    0.15    + Dolfineps);
% gamma_7_rect=myrectangle(-Dolfineps+0.0     , 0.5     + Dolfineps ,-Dolfineps + 0.0    ,    0.0     + Dolfineps);
% 
% %% plot gamma rectangles
% plot(gamma_0_rect(:,1),gamma_0_rect(:,2),'Color',Colors(1,:))
% plot(gamma_1_rect(:,1),gamma_1_rect(:,2),'Color',Colors(2,:))
% plot(gamma_2_rect(:,1),gamma_2_rect(:,2),'Color',Colors(3,:))
% plot(gamma_3_rect(:,1),gamma_3_rect(:,2),'Color',Colors(4,:))
% plot(gamma_4_rect(:,1),gamma_4_rect(:,2),'Color',Colors(5,:))
% plot(gamma_5_rect(:,1),gamma_5_rect(:,2),'Color',Colors(6,:))
% plot(gamma_6_rect(:,1),gamma_6_rect(:,2),'Color',Colors(7,:))
% plot(gamma_7_rect(:,1),gamma_7_rect(:,2),'Color',Colors(8,:))

%% define edge numbers rectangles 
edge_1_rect=myrectangle(-Dolfineps+0.0     , 0.5     + Dolfineps ,-Dolfineps + 0.0    ,    0.0     + Dolfineps);
edge_2_rect=myrectangle(-Dolfineps+0.5     , 0.5     + Dolfineps ,-Dolfineps + 0.0    ,    0.15    + Dolfineps);
edge_3_rect=myrectangle(-Dolfineps+0.19607 , 0.5     + Dolfineps ,-Dolfineps + 0.15   ,    0.22151 + Dolfineps);
edge_4_rect=myrectangle(-Dolfineps+0.11    , 0.19607 + Dolfineps ,-Dolfineps + 0.22151,    0.284   + Dolfineps);
edge_5_rect=myrectangle(-Dolfineps+0.09    , 0.11    + Dolfineps ,-Dolfineps + 0.284  ,    0.3554  + Dolfineps);
edge_6_rect=myrectangle(-Dolfineps+0.09    , 0.11    + Dolfineps ,-Dolfineps + 1.03778,    1.1     + Dolfineps);
edge_7_rect=myrectangle(-Dolfineps+0.11    , 0.1797  + Dolfineps ,-Dolfineps + 1.1    ,    1.14319 + Dolfineps);
edge_8_rect=myrectangle(-Dolfineps+0.1797  , 0.32441 + Dolfineps ,-Dolfineps + 1.14319,    1.16666 + Dolfineps);
edge_9_rect=myrectangle(-Dolfineps+0.32441 , 0.37482 + Dolfineps ,-Dolfineps + 1.16666,    1.20249 + Dolfineps);
edge_10_rect=myrectangle(-Dolfineps+0.37482, 0.3826  + Dolfineps ,-Dolfineps + 1.20249,    1.245   + Dolfineps);
edge_11_rect=myrectangle(-Dolfineps+0.345  , 0.3826  + Dolfineps ,-Dolfineps + 1.245  ,    1.5303  + Dolfineps);
edge_12_rect=myrectangle(-Dolfineps+0.328  , 0.345   + Dolfineps ,-Dolfineps + 1.5303 ,    1.57    + Dolfineps);
edge_13_rect=myrectangle(-Dolfineps+0.2655 , 0.328   + Dolfineps ,-Dolfineps + 1.57   ,    1.6     + Dolfineps);
edge_14_rect=myrectangle(-Dolfineps+0.0    , 0.2655  + Dolfineps ,-Dolfineps + 1.6    ,    1.6     + Dolfineps);
edge_15_rect=myrectangle(-Dolfineps+0.09   , 0.09    + Dolfineps ,-Dolfineps + 0.3544 ,    0.73    + Dolfineps);
edge_16_rect=myrectangle(-Dolfineps+0.09   , 0.09    + Dolfineps ,-Dolfineps + 0.73   ,    1.03778 + Dolfineps);
edge_17_rect=myrectangle(-Dolfineps+0.0    , 0.0     + Dolfineps ,-Dolfineps + 0.0    ,    0.3     + Dolfineps);
edge_18_rect=myrectangle(-Dolfineps+0.0    , 0.0     + Dolfineps ,-Dolfineps + 0.3    ,    0.625   + Dolfineps);
edge_19_rect=myrectangle(-Dolfineps+0.0    , 0.0     + Dolfineps ,-Dolfineps + 0.625  ,    1.45    + Dolfineps);
edge_20_rect=myrectangle(-Dolfineps+0.0    , 0.0     + Dolfineps ,-Dolfineps + 1.45   ,    1.6     + Dolfineps);


%% plot edge numbers rectangles
plot(edge_1_rect(:,1),edge_1_rect(:,2),'Color',Colors(1,:))
plot(edge_2_rect(:,1),edge_2_rect(:,2),'Color',Colors(2,:))
plot(edge_3_rect(:,1),edge_3_rect(:,2),'Color',Colors(3,:))
plot(edge_4_rect(:,1),edge_4_rect(:,2),'Color',Colors(4,:))
plot(edge_5_rect(:,1),edge_5_rect(:,2),'Color',Colors(5,:))
plot(edge_6_rect(:,1),edge_6_rect(:,2),'Color',Colors(6,:))
plot(edge_7_rect(:,1),edge_7_rect(:,2),'Color',Colors(7,:))
plot(edge_8_rect(:,1),edge_8_rect(:,2),'Color',Colors(8,:))
plot(edge_9_rect(:,1),edge_9_rect(:,2),'Color',Colors(9,:))
plot(edge_10_rect(:,1),edge_10_rect(:,2),'Color',Colors(10,:))
plot(edge_11_rect(:,1),edge_11_rect(:,2),'Color',Colors(11,:))
plot(edge_12_rect(:,1),edge_12_rect(:,2),'Color',Colors(12,:))
plot(edge_13_rect(:,1),edge_13_rect(:,2),'Color',Colors(13,:))
plot(edge_14_rect(:,1),edge_14_rect(:,2),'Color',Colors(14,:))
plot(edge_15_rect(:,1),edge_15_rect(:,2),'Color',Colors(15,:))
plot(edge_16_rect(:,1),edge_16_rect(:,2),'Color',Colors(16,:))
plot(edge_17_rect(:,1),edge_17_rect(:,2),'Color',Colors(17,:))
plot(edge_18_rect(:,1),edge_18_rect(:,2),'Color',Colors(18,:))
plot(edge_19_rect(:,1),edge_19_rect(:,2),'Color',Colors(19,:))
plot(edge_20_rect(:,1),edge_20_rect(:,2),'Color',Colors(20,:))













boundary
%plot(Boundary_mesh_2ref(:,1),Boundary_mesh_2ref(:,2),'ro')
%plot(Boundary_mesh_1ref(:,1),Boundary_mesh_1ref(:,2),'go')
plot(Boundary_macro(:,1),Boundary_macro(:,2),'o')
