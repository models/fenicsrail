from dolfin import *
import numpy



def count_unique(keys):
    """count every element in numpy array and return a rank list"""
    uniq_keys = numpy.unique(keys)
    bins = uniq_keys.searchsorted(keys)
    return zip(uniq_keys, numpy.bincount(bins))


def get_boundary(edge_numbers,number):
    """return array with coordinates of edge with number"""
    coord=numpy.array([])
    mesh = edges_numbers.mesh()
    for edge in edges(mesh):
        if edge_numbers.array()[edge.index()]==number:
            for vert in vertices(edge):
                coord = numpy.append(coord,mesh.coordinates()[vert.index()])

    return coord.reshape(coord.size/2,2)


def is_edge_on(edge,on_piece,mesh):
    """edge and function on_piece as argument return true if every for vertex of edge on_piece(vertex) returns true"""
    #return all(map(on_piece,vertices(edge)))
    return all(map(lambda(v): on_piece(v,mesh),vertices(edge)))


def refine_mesh_function(ref_mesh,edge_numbers):
    """Compute MeshFunction for the refined mesh with respect to the original mesh and corresponding MeshFunction edge_numbers.
    Compute also a parent facet Meshfunction.

    Args:
        ref_mesh (Mesh): refined mesh
        edge_numbers (MeshFunction): Should correct index edges from 1 to 20 on the boundary and 0 for inner edges.

    Returns:
        MeshFunction: index edges of the refined mesh ref_mesh from 1 to 20 on the boundary and 0 for inner edges.
        MeshFunction: parent facet function maps from edges of refined mesh to edges of mesh

    """

    ref_edge_numbers    = MeshFunction('size_t',ref_mesh, 1, CONST.EDGES_GAMMA_INNER[0])
    parent_facet        = MeshFunction('size_t',ref_mesh,1)

    if   CONST.DOLFIN_VERSION   ==  '1.2.0':
        parent_facet = ref_mesh.data().mesh_function('parent_facet')

    elif CONST.DOLFIN_VERSION   ==  '1.3.0':
        parent_facet_array=ref_mesh.data().array('parent_facet',1)

        for ref_edge in edges(ref_mesh):
            parent_facet[ref_edge] = parent_facet_array[ref_edge.index()]
    else:
        warnings.warn('Code only tested with Dolfin 1.2.0 and Dolfin 1.3.0. Code for your Dolfin version not checked')
        parent_facet_array=ref_mesh.data().array('parent_facet',1)

        for ref_edge in edges(ref_mesh):
            parent_facet[ref_edge] = parent_facet_array[ref_edge.index()]



    for ref_edge in edges(ref_mesh):
        #check if edge is a not a new edge
        #new edges have value outside
        if 0<=parent_facet[ref_edge]<=edge_numbers.size():
            ref_edge_numbers[ref_edge]=edge_numbers[parent_facet[ref_edge]]

    return ref_edge_numbers, parent_facet


def refine_rail(mesh,edge_numbers):
    """Refine mesh, compute MeshFunction, project Boundaries and returns it.

    """
    #dummy_marker only for computing of parent_facet mesh_function
    dummy_marker = dolfin.MeshFunction('bool',mesh,mesh.topology().dim(),True)
    parameters['refinement_algorithm']=CONST.REFINEMENT_ALGORITHM
    ref_mesh=refine(mesh, dummy_marker)

    ref_edge_numbers, parent_facet = refine_mesh_function(ref_mesh, edge_numbers)


    Ball_project(CONST.INDEX_BALL1,ref_mesh,ref_edge_numbers)
    Ball_project(CONST.INDEX_BALL2,ref_mesh,ref_edge_numbers)
    Ball_project(CONST.INDEX_BALL3,ref_mesh,ref_edge_numbers)
    Ball_project(CONST.INDEX_BALL4,ref_mesh,ref_edge_numbers)

    return ref_mesh, ref_edge_numbers, parent_facet


def compute_refined_meshes():
    """Computes refined Meshes and MeshFunctions.

    Read macro Mesh and MeshFunction for edge numbers.
    Compute MeshFunction for gamma_numbers for macro Mesh and MeshFunction edge_numbers.
    Then the mesh will refined, MeshFunctions for edge_numbers, gamma_numbers and parent_facet are computed and
    stored.
    """

    ####################MACRO DATA####################
    #read mesh function from file
    #read MeshFunction edge_numbers defined on Edges from file (ref Abb1.1 p.16)
    #compute MeshFunction gamma_numbers define on Edges from file (ref Abb1.1 p.16)
    #plot all and save plots
    mesh = dolfin.Mesh(CONST.MACRO_MESH_FILE)
    edge_numbers = dolfin.MeshFunction('size_t',mesh,CONST.MACRO_EDGE_NUMBERS_FILE)
    gamma_numbers= edge_numbers_to_gamma_numbers(mesh, edge_numbers)

    File(CONST.MACRO_MESH_VTK_FILE,'compressed')<<mesh
    File(CONST.MACRO_GAMMA_NUMBERS_VTK_FILE,'compressed')<<gamma_numbers
    File(CONST.MACRO_GAMMA_NUMBERS_VTK_FILE,'compressed')<<gamma_numbers
    File(CONST.MACRO_EDGE_NUMBERS_VTK_FILE,'compressed')<<edge_numbers



    ####################1. REFINE####################
    ref1_mesh, ref1_edge_numbers, ref1_parent_facet = refine_rail(mesh,edge_numbers)
    ref1_gamma_numbers=edge_numbers_to_gamma_numbers(ref1_mesh, ref1_edge_numbers)



    File(CONST.REFN_MESH_FILE %(1,1))<<ref1_mesh
    File(CONST.REFN_EDGE_NUMBERS_FILE %(1,1))<<ref1_edge_numbers
    File(CONST.REFN_GAMMA_NUMBERS_FILE %(1,1))<<ref1_gamma_numbers
    File(CONST.REFN_PARENT_FACET_FILE %(1,1))<<ref1_parent_facet


    File(CONST.REFN_EDGE_NUMBERS_VTK_FILE   %(1,1), 'compressed')<<ref1_edge_numbers
    File(CONST.REFN_GAMMA_NUMBERS_VTK_FILE  %(1,1), 'compressed')<<ref1_gamma_numbers
    File(CONST.REFN_PARENT_FACET_VTK_FILE   %(1,1), 'compressed')<<ref1_parent_facet



    ####################MACRO FOR REFINING####################
    refiner=Template('\
ref${NR}_mesh, ref${NR}_edge_numbers, ref${NR}_parent_facet = refine_rail(ref${LAST}_mesh,ref${LAST}_edge_numbers)      \n\
ref${NR}_gamma_numbers=edge_numbers_to_gamma_numbers(ref${NR}_mesh, ref${NR}_edge_numbers)       \n\
File(CONST.REFN_MESH_FILE %(${NR},${NR}))<<ref${NR}_mesh                            \n\
File(CONST.REFN_EDGE_NUMBERS_FILE %(${NR},  ${NR}))<<ref${NR}_edge_numbers            \n\
File(CONST.REFN_GAMMA_NUMBERS_FILE %(${NR}, ${NR}))<<ref${NR}_gamma_numbers          \n\
File(CONST.REFN_PARENT_FACET_FILE  %(${NR}, ${NR}))<<ref${NR}_parent_facet')

    plotter=Template('\
File(CONST.REFN_MESH_VTK_FILE %(${NR}, ${NR}), \'compressed\')<<ref${NR}_mesh    \n\
File(CONST.REFN_EDGE_NUMBERS_VTK_FILE %(${NR}, ${NR}), \'compressed\')<<ref${NR}_edge_numbers    \n\
File(CONST.REFN_GAMMA_NUMBERS_VTK_FILE %(${NR},${NR}), \'compressed\')<<ref${NR}_gamma_numbers \n\
File(CONST.REFN_PARENT_FACET_VTK_FILE  %(${NR}, ${NR}), \'compressed\')<<ref${NR}_parent_facet')
    ####################FURTHER REFINE####################
    exec(refiner.substitute(NR=2,LAST=1))
    exec(plotter.substitute(NR=2,LAST=1))

    exec(refiner.substitute(NR=3,LAST=2))
    exec(plotter.substitute(NR=3,LAST=2))

    exec(refiner.substitute(NR=4,LAST=3))
    exec(plotter.substitute(NR=4,LAST=3))

    exec(refiner.substitute(NR=5,LAST=4))
    exec(plotter.substitute(NR=5,LAST=4))

    exec(refiner.substitute(NR=6,LAST=5))
    exec(plotter.substitute(NR=6,LAST=5))



def compute_edge_numbers_function(mesh):
    """compute mesh function defined on edges which return the edge number"""

    #initilize all edges as inner edge
    edge_numbers = dolfin.MeshFunction('size_t',mesh,1,CONST.EDGE_INNER)
    #iterate over edges if edge is on boundary assign correct edge number
    for e in edges(mesh):
        if edge_on_boundary(e):
            if  (is_edge_on(e, CONST.on_edge_1_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_1
            elif(is_edge_on(e, CONST.on_edge_2_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_2
            elif(is_edge_on(e, CONST.on_edge_3_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_3
            elif(is_edge_on(e, CONST.on_edge_4_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_4
            elif(is_edge_on(e, CONST.on_edge_5_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_5
            elif(is_edge_on(e, CONST.on_edge_6_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_6
            elif(is_edge_on(e, CONST.on_edge_7_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_7
            elif(is_edge_on(e, CONST.on_edge_8_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_8
            elif(is_edge_on(e, CONST.on_edge_9_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_9
            elif(is_edge_on(e, CONST.on_edge_10_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_10
            elif(is_edge_on(e, CONST.on_edge_11_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_11
            elif(is_edge_on(e, CONST.on_edge_12_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_12
            elif(is_edge_on(e, CONST.on_edge_13_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_13
            elif(is_edge_on(e, CONST.on_edge_14_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_14
            elif(is_edge_on(e, CONST.on_edge_15_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_15
            elif(is_edge_on(e, CONST.on_edge_16_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_16
            elif(is_edge_on(e, CONST.on_edge_17_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_17
            elif(is_edge_on(e, CONST.on_edge_18_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_18
            elif(is_edge_on(e, CONST.on_edge_19_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_19
            elif(is_edge_on(e, CONST.on_edge_20_boundary, mesh)):
                edge_numbers[e]=CONST.EDGE_20
            else:
                raise ValueError('Edge with index: %d could not assigned to boundary part' %(e.index()))

    return edge_numbers


"""inequalities describe edge number boundary parts ref. Abb 1.1"""
"""use only function if you know that v lies on boundary of mesh"""
def on_edge_1_boundary(v):
    """check if vertex v is on boundary with edge number 1 ref. Abb 1.1"""
    return 0.0 <= v.x(0) <= 0.5 and v.x(1)==0.0

def on_edge_2_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 2 ref. Abb 1.1"""
    return v.x(0)==0.5 and 0.0 <= v.x(1) <= 0.15

def on_edge_3_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 3 ref. Abb 1.1"""
    return 0.19607 <= v.x(0) <= 0.5 and  0.15 <= v.x(1) <= 0.22151

def on_edge_4_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 4 ref. Abb 1.1"""
    return 0.11 <= v.x(0) <= 0.19607 and  0.22151 <= v.x(1) <= 0.284

def on_edge_5_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 5 ref. Abb 1.1"""
    return 0.09 <= v.x(0) <= 0.11 and  0.284 <= v.x(1) <= 0.35544

def on_edge_6_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 6 ref. Abb 1.1"""
    return 0.09 <= v.x(0) <= 0.11 and  1.03778 <= v.x(1) <= 1.1

def on_edge_7_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 7 ref. Abb 1.1"""
    return 0.11 <= v.x(0) <= 0.1797 and  1.1 <= v.x(1) <= 1.14319

def on_edge_8_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 8 ref. Abb 1.1"""
    return 0.1797 <= v.x(0) <= 0.32441 and  1.14319 <= v.x(1) <= 1.16666

def on_edge_9_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 9 ref. Abb 1.1"""
    return 0.32441 <= v.x(0) <= 0.37482 and  1.16666 <= v.x(1) <= 1.20249

def on_edge_10_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 10 ref. Abb 1.1"""
    return 0.37482 <= v.x(0) <= 0.3826  and  1.20249 <= v.x(1) <= 1.245

def on_edge_11_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 11 ref. Abb 1.1"""
    return 0.345 <= v.x(0) <= 0.3826  and  1.245 <= v.x(1) <= 1.5303

def on_edge_12_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 12 ref. Abb 1.1"""
    return 0.328 <= v.x(0) <= 0.345   and  1.5303 <= v.x(1) <= 1.57

def on_edge_13_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 13 ref. Abb 1.1"""
    return 0.2655 <= v.x(0) <= 0.328   and  1.57 <= v.x(1) <= 1.6

def on_edge_14_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 14 ref. Abb 1.1"""
    return 0.0 <= v.x(0) <= 0.2655  and  v.x(1) == 1.6

def on_edge_15_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 15 ref. Abb 1.1"""
    return v.x(0)==0.09 and  0.35544 <= v.x(1) <= 0.73

def on_edge_16_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 16 ref. Abb 1.1"""
    return v.x(0)==0.09 and  0.73 <= v.x(1) <= 1.03778

def on_edge_17_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 17 ref. Abb 1.1"""
    return v.x(0) == 0.0  and  0.0 <= v.x(1) <= 0.3

def on_edge_18_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 18 ref. Abb 1.1"""
    return v.x(0) == 0.0  and  0.3 <= v.x(1) <= 0.625

def on_edge_19_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 19 ref. Abb 1.1"""
    return v.x(0) == 0.0  and  0.625 <= v.x(1) <= 1.45

def on_edge_20_boundary(v, mesh):
    """check if vertex v is on boundary with edge number 19 ref. Abb 1.1"""
    return v.x(0) == 0.0  and  1.45 <= v.x(1) <= 1.6




class Gamma_0_Boundary(dolfin.SubDomain):
    """define boundary gamma_0 with edges {17,18,19,20} ref. Abb 1.1"""
    def inside(self, x, on_boundary):
        return on_boundary and dolfin.near(x[0],0.0) and dolfin.between(x[1],(0.0,1.6))


class Gamma_1_Boundary(dolfin.SubDomain):
    """defin boundary_gamma_1 with edges {12,13,14} ref. Abb 1.1"""
    def inside(self, x, on_boundary):
        return on_boundary and dolfin.between(x[0],(0, 0.34872)) and dolfin.between(x[1],(1.50207,1.6))


class Gamma_2_Boundary(dolfin.SubDomain):
    """define boundary gamma_2 with edges {11} ref. Abb 1.1"""
    def inside(self, x, on_boundary):
        return on_boundary and dolfin.between(x[0],(0.345, 0.3826)) and dolfin.between(x[1],(1.245, 1.5303))


class Gamma_3_Boundary(dolfin.SubDomain):
    """define boundary gamma_3 with edges {6,7,8,9,10} ref. Abb 1.1"""
    def inside(self, x, on_boundary):
        return on_boundary and dolfin.between(x[0], (0.09 ,0.3826)) and dolfin.between(x[1],( 1.03778,1.245))

class Gamma_4_Boundary(dolfin.SubDomain):
    """define boundary gamma_4 with edges {15,16} ref. Abb 1.1"""
    def inside(self,x,on_boundary):
        return on_boundary and dolfin.near(x[0],0.09) and dolfin.between(x[1],(0.35544,1.03778))

class Gamma_5_Boundary(dolfin.SubDomain):
    """define boundary gamma_5 with edges {3,4,5} ref. Abb 1.1"""
    def inside(self,x,on_boundary):
        return on_boundary and dolfin.between(x[0],(0.09,0.5)) and dolfin.between(x[1],(0.15,0.35544))

class Gamma_6_Boundary(dolfin.SubDomain):
    """define boundary gamma_6 with edges {2} ref. Abb 1.1"""
    def inside(self,x,on_boundary):
        return on_boundary and dolfin.near(x[0],0.5) and dolfin.between(x[1],(0.0,0.15))

class Gamma_7_Boundary(dolfin.SubDomain):
    """define boundary gamma_7 with edges {1} ref. Abb 1.1"""
    def inside(self,x,on_boundary):
        return on_boundary and dolfin.near(x[1],0.0) and dolfin.between(x[1],(0.0,0.5))


def Ball1_project(mesh,edge_numbers):
    """projection function for the vertices on edges {12,13} ref. Abb 1.1, p.50, p.81"""
    rad =    0.08019162478295
    m   =   [0.2655, 1.5198]
    for edge in edges(mesh):
        if edge_numbers[edge]==12 or edge_numbers[edge]==13:
            for vert in vertices(edge):
                x0 = mesh.coordinates()[vert.index(),0]
                x1 = mesh.coordinates()[vert.index(),1]
                norm = sqrt((x0 -m[0])**2 + (x1 - m[1])**2)
                mesh.coordinates()[vert.index(),0]=m[0] + (rad / norm)*(x0 - m[0])
                mesh.coordinates()[vert.index(),1]=m[1] + (rad / norm)*(x1 - m[1])


def Ball2_project(mesh,edge_numbers):
    """projection function for the vertices on edges {4,5} ref. Abb 1.1, p.50, p.81"""
    rad =    0.13758140667677
    m   =   [0.22758, 0.35544]
    for edge in edges(mesh):
        if edge_numbers[edge]==4 or edge_numbers[edge]==5:
            for vert in vertices(edge):
                x0 = mesh.coordinates()[vert.index(),0]
                x1 = mesh.coordinates()[vert.index(),1]
                norm = sqrt((x0 -m[0])**2 + (x1 - m[1])**2)
                mesh.coordinates()[vert.index(),0]=m[0] + (rad / norm)*(x0 - m[0])
                mesh.coordinates()[vert.index(),1]=m[1] + (rad / norm)*(x1 - m[1])

def Ball3_project(mesh,edge_numbers):
    """projection function for the vertices on edges {6,7} ref. Abb 1.1, p.50, p.81"""
    rad =    0.10679592032499
    m   =   [0.196796, 1.03778]
    for edge in edges(mesh):
        if edge_numbers[edge]==6 or edge_numbers[edge]==7:
            for vert in vertices(edge):
                x0 = mesh.coordinates()[vert.index(),0]
                x1 = mesh.coordinates()[vert.index(),1]
                norm = sqrt((x0 -m[0])**2 + (x1 - m[1])**2)
                mesh.coordinates()[vert.index(),0]=m[0] + (rad / norm)*(x0 - m[0])
                mesh.coordinates()[vert.index(),1]=m[1] + (rad / norm)*(x1 - m[1])

def Ball4_project(mesh,edge_numbers):
    """projection function for the vertices on edges {9,10} ref. Abb 1.1, p.50, p.81"""
    rad =    0.07005044877845
    m   =   [0.3132, 1.23581]
    for edge in edges(mesh):
        if  edge_numbers[edge]==9 or  edge_numbers[edge]==10:
            for vert in vertices(edge):
                x0 = mesh.coordinates()[vert.index(),0]
                x1 = mesh.coordinates()[vert.index(),1]
                norm = sqrt((x0 -m[0])**2 + (x1 - m[1])**2)
                mesh.coordinates()[vert.index(),0]=m[0] + (rad / norm)*(x0 - m[0])
                mesh.coordinates()[vert.index(),1]=m[1] + (rad / norm)*(x1 - m[1])


"""
    mesh_boundary_edge_ind      = np.zeros(mesh.num_edges(),dtype=np.int)
    i=0
    for e in edges(mesh):
        if edge_on_boundary(e):
            mesh_boundary_edge_ind[i]=e.index()
            i+=1
    mesh_boundary_edge_ind=mesh_boundary_edge_ind[0:i]
"""


"""
    mmwrite('eqnAm',-1*eqn.A_)
    mmwrite('eqnA',eqn.A_)

    mmwrite('eqnE',eqn.E_)
    mmwrite('eqnB',eqn.B)
    mmwrite('eqnC',eqn.C)

    Adense = eqn.A_.toarray()
    Edense = eqn.E_.toarray()
    e,_  =   eig(Adense)
    print 'Max Eigenv.:',max(e), 'Min Eigenv:', min(e)
    e,_  =   eig(Edense)
    print 'Max Eigenv.:',max(e), 'Min Eigenv:', min(e)

    condition = np.linalg.cond(Adense)
    print 'Cond:', condition

    cond = np.linalg.cond(Edense)
    print 'Cond:', condition
"""


