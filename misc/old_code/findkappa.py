# -*- coding: utf-8 -*-
import raillog
import constants as CONST
from scipy.io import savemat
from dolfin import Constant, Measure, Mesh, MeshFunction, Function, TrialFunction,\
TestFunction, inner, grad, assemble, dx,  FunctionSpace, project, solve, File


def ODE_ansatz_linear(mesh_file, gamma_numbers_file, integrator):
    """Precomputation (4.2 Die ungesteuerte Referenzrechnung) p.47. with ODE-Ansatz.
    Boundary condition p.18 (1.3) with q_k == kappa.
    Use constants coefficients rho, c, lambda.

    Function takes the constants from wip_constants, define a weak formulation and solve it with ODE-Ansatz and numerical integration.
    Save temperature in vtk file and temperature at the end in mat file s.a. constants.py

     Args:
        mesh_file           (String):        points to the .xml mesh file (mesh folder)
        gamma_numbers_file  (String):        points to the .xml MeshFunction file (mesh folder)
        integrator          (String):        CONST.EULER_IMPLICIT, CONST.EULER_EXPLICIT, CONST.CRANK_NICOLSON

    Returns:

    Raises: ValueError If integrator is not CONST.EULER_IMPLICIT, CONST.EULER_EXPLICIT, CONST.CRANK_NICOLSON

    Information:

    """
    #print CONST.CONST_KAPPA

    #set constants
    const_rho           =   Constant(CONST.CONST_RHO)
    const_c             =   Constant(CONST.CONST_C)
    const_kappa         =   Constant(CONST.CONST_KAPPA)
    const_alpha         =   Constant(CONST.CONST_ALPHA)
    const_u_ext         =   Constant(CONST.CONST_UEXT)

    #read mesh and gamma_numbers
    mesh                =   Mesh(mesh_file)
    gamma_numbers       =   MeshFunction('size_t',mesh,gamma_numbers_file)

    #define function space linear Lagrange P1
    V                   =   FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    #set time step size and simulation time and t for iterating over time
    delta_t             =   CONST.T_DELTA
    T                   =   CONST.T_END
    t                   =   0

    #the solution u and the derivative du_dt
    u                   =   TrialFunction(V)
    du_dt               =   TrialFunction(V)
    v                   =   TestFunction(V)

    #define terms of weak formulations
    ds                  =   Measure('ds')[gamma_numbers]
    M_var               =   du_dt*v*dx
    S_var               =   const_alpha*inner(grad(u),grad(v))*dx
    M_GAMMA_var         =   sum([const_kappa/(const_c*const_rho)*u*v*ds(i)              for i in range(1,8)])
    B_var               =   sum([const_kappa/(const_c*const_rho)*const_u_ext*v*ds(i)    for i in range(1,8)])

    #assemble matrices
    M                   =   assemble(M_var)
    S                   =   assemble(S_var)
    M_GAMMA             =   assemble(M_GAMMA_var)
    B                   =   assemble(B_var)
    delta_tB            =   delta_t*B

    #define u0 as constant temperature
    u_0                 =   Constant(CONST.CONST_U0)

    #project u_0 down to  function space
    u_k_1               =   project(u_0,V)

    #the solution
    u                   =   Function(V)

    #M_iter*u^{k}=A_iter*u{k-1}+delta_t*B
    if integrator   == CONST.EULER_IMPLICIT:
        M_iter          =   M+delta_t*(S+M_GAMMA)
        A_iter          =   M
    elif integrator ==CONST.EULER_EXPLICIT:
        M_iter          =   M
        A_iter          =   M-delta_t*(S+M_GAMMA)
    elif integrator == CONST.CRANK_NICOLSON:
        M_iter          =   M+delta_t*0.5*(S+M_GAMMA)
        A_iter          =   M-delta_t*0.5*(S+M_GAMMA)
    else:
        raise ValueError('integration method: %s is not implemented'% (integrator))


    #set name in vtk file
    u_k_1.rename(CONST.VTK_FIELD_NAME,CONST.VTK_LABEL_NAME)



    #start simulation
    while t<=T:
        #compute rhs and solve equation system
        b = A_iter*u_k_1.vector()+delta_tB
        solve(M_iter,u.vector(),b)
        #increment time and u_k_1<-u_k
        t+=delta_t
        u_k_1.assign(u)
    #end simulation


    print  "|0.934-umax|", abs(0.934-u.vector().max()),"|0.756-umin|", abs(0.756-u.vector().min()), "kappa",CONST.CONST_KAPPA


if __name__ == '__main__':

    kappa=17.5726
    dkappa=0.000001

    while kappa<=17.57263:

        CONST.CONST_KAPPA=kappa

        ODE_ansatz_linear(CONST.MACRO_MESH_FILE,CONST.MACRO_GAMMA_NUMBERS_FILE,CONST.EULER_IMPLICIT)

        kappa+=dkappa
