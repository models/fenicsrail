import numpy as np
import constants as CONST
import dolfin
from dolfin import *
from dolfin_utils import *
from dolfin.cpp.la import uBLASDenseMatrix
from pycmess import lrnm, equation, options
from scipy.linalg import cholesky, solve_triangular, cho_solve
from scipy.io import savemat
#import pdb


def LQR(mesh_file, gamma_numbers_file, integrator,C_func,Q_,R_):
    """LQR problem solver (3.1.1 ODE Zugang) p.33. with ODE-Ansatz


    Function takes the constants from wip_constants, define a weak formulation and simulate cooling process until
    maximum temperature is lower than CONST_U_INIT (defined in constants.py).
    Then the optimal feedback is computed by solving the Riccatti Equation from the LQR Problem with the mess python interface.

    Save temperature in vtk file and temperature at the end in mat file see constants.py

     Args:
        mesh_file           (String):        points to the .xml mesh file (mesh folder)
        gamma_numbers_file  (String):        points to the .xml MeshFunction file (mesh folder)
        integrator          (String):        CONST.EULER_IMPLICIT, CONST.EULER_EXPLICIT, CONST.CRANK_NICOLSON
        C_func              (Function):      return Output Matrix C for given Number of Vertices (s.a. constants.py)
        Q_                  (Matrix):        Cost matrix (s.a. constants.py)
        R_                  (Matrix):        Cost matrix (s.a. constants.py)



    Returns:

    Raises:

    Information:

    """


    #define constants
    const_rho       =   Constant(CONST.CONST_RHO)
    const_c         =   Constant(CONST.CONST_C)
    const_kappa     =   Constant(CONST.CONST_KAPPA)
    const_alpha     =   Constant(CONST.CONST_ALPHA)
    const_u_ext     =   CONST.CONST_UEXT

    #read mesh file and mesh function for gamma boundary
    mesh = dolfin.Mesh(mesh_file)
    gamma_numbers = dolfin.MeshFunction('size_t',mesh,gamma_numbers_file)

    #define function space linear lagrange elements P1
    V = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    #set time step size and simulation time and t for iterating over time
    delta_t         = CONST.T_DELTA
    T               = CONST.T_END
    t               = delta_t

    #the solution u and the derivative du_dt
    u               = TrialFunction(V)
    du_dt           = TrialFunction(V)
    v               = TestFunction(V)

    #define terms of weak formulations
    M_var           = du_dt*v*dx
    S_var           = const_alpha*inner(grad(u),grad(v))*dx
    M_GAMMA_var     = sum([const_kappa/(const_c*const_rho)*u*v*ds(i) for i in range(1,8)])
    Buext_var       = sum([const_u_ext*const_kappa/(const_c*const_rho)*v*ds(i) for i in range(1,8)])
    B_vars          = [const_u_ext*const_kappa/(const_c*const_rho)*v*ds(i) for i in range(1,8)]

    #assemble matrices
    M               = assemble(M_var)
    S               = assemble(S_var)
    M_GAMMA         = assemble(M_GAMMA_var, exterior_facet_domains=gamma_numbers)
    Buext           = assemble(Buext_var, exterior_facet_domains=gamma_numbers)
    delta_tBuext    = delta_t*Buext
    B_cols          = map(lambda(x):assemble(x,exterior_facet_domains=gamma_numbers),B_vars)

    #construct dense matrix from B_cols
    B               = uBLASDenseMatrix(mesh.num_vertices(),7)
    B.zero()
    for i in range(7):
        B.set(B_cols[i].array(),np.arange(mesh.num_vertices(),dtype=np.intc),np.array([i],dtype=np.intc))


    #M_iter*u^{k}=A_iter*u{k-1}+delta_t*B
    if integrator   == CONST.EULER_IMPLICIT:
        M_iter      = M+delta_t*(S+M_GAMMA)
        A_iter      = M
    elif integrator ==CONST.EULER_EXPLICIT:
        M_iter      = M
        A_iter      = M-delta_t*(S+M_GAMMA)
    elif integrator == CONST.CRANK_NICOLSON:
        M_iter      = M+delta_t*1/2*(S+M_GAMMA)
        A_iter      = M-delta_t*1/2*(S+M_GAMMA)
    else:
        raise ValueError('integration method: %s is not implemented'% (integrator))



    #define u0 as constant temperature
    u_0             = Constant(CONST.CONST_U0)

    #project u_0 down to  function space
    u_k_1           = project(u_0,V)

    #the solution
    u_k             = Function(V)


    #vtk_file = File(CONST.LQR_VTK_FILENAME %(str(mesh.num_vertices())+'_'+integrator+'_'+C_func.__name__))



    ########anlaufrechnung#########
    #for debug info
    old_percent=-CONST.PERCENT_STEP
    #for vtk sample
    old_time = -CONST.VTK_SAMPLE_TIME

    u_k_1.rename(CONST.VTK_FIELD_NAME,CONST.VTK_LABEL_NAME)
    u_max=1

    while CONST.CONST_U_INIT<u_max and t<T:

        b = A_iter*u_k_1.vector()+delta_tBuext
        dolfin.solve(M_iter,u_k.vector(),b)

        if(t-old_time)>=CONST.VTK_SAMPLE_TIME:
            #print "Write VTK Dump at time:%.2fs"%(t/100)
            #vtk_file << u_k_1
            old_time=t

        u_max = u_k_1.vector().max()
        u_min = u_k_1.vector().min()
        percent = (t/T)*100

        if((percent-old_percent)>=CONST.PERCENT_STEP):
            print CONST.PERCENT_OUTPUT %(percent,t/100,u_max,u_min)
            old_percent=percent

        t+=delta_t
        u_k_1.assign(u_k)



    ######compute optimal feedback################

    #define cost functional and output matrix
    Q = Q_
    R = R_
    C = C_func(mesh.num_vertices(), CONST.get_dof_to_vertex_map(V, mesh))

    #Q = CONST.LQR_Q
    #R = CONST.LQR_R
    #C = CONST.LQR_C_60(mesh.num_vertices())

    #parameters for newton method for solving ARE
    opt = options()
    opt.adi.type='C'#MESS_OP_TRANSPOSE

    #set up Matrices for ARE p.41 p.26 (2.21)
    eqn         = equation()
    eqn.A_      = -1*(S+M_GAMMA).sparray()
    eqn.E_      = M.sparray()

    # set Q and R correct in ARE and convert to matrix
    R_chol_L    = cholesky(R,lower=True)

    print R_chol_L.shape
    print B.array().shape
    eqn.B       = solve_triangular(R_chol_L, B.array().T, 'N', lower=True).T

    Q_chol_L    = cholesky(Q,lower=True)
    eqn.C       = np.dot( Q_chol_L.T, C.toarray())

    # solve ARE get get low rank factor for X = np.dot(Z[0],Z[0].T)
    Z           = lrnm(eqn,opt)

    #np.linalg.norm(eqn.A_.T*Z[0]*Z[0].T*eqn.E_  + eqn.E_.T*Z[0]*Z[0].T*eqn.A_ -  eqn.E_.T*Z[0]*Z[0].T*eqn.B*eqn.B.T*Z[0]*Z[0].T*eqn.E_+eqn.C.T*Q*eqn.C)

    ####compute temperature with optimal feedback
    delta_tB=delta_t*B


    print 'Shape Z:',Z[0].shape

    Z = np.asarray(Z[0])

    while t<T:

        #compute control q by put last temperature in feedback
        ZTu_k_1 =   np.dot( Z.T,  u_k_1.vector().array() )
        BTZ     =   np.dot( B.array().T, Z )
        q       =   cho_solve((R_chol_L,True), -1*np.dot(BTZ,ZTu_k_1) )
        #q is a numpy array

        #compute new right hand side
        #pdb.set_trace()
        b[:]= np.dot(delta_tB.array(),q)[:]
        b = A_iter*u_k_1.vector()+b
        dolfin.solve(M_iter,u_k.vector(),b)

        if(t-old_time)>=CONST.VTK_SAMPLE_TIME:
            #print "Write VTK Dump at time:%.2fs"%(t/100)
            #vtk_file << u_k_1
            old_time=t

        u_max = u_k_1.vector().max()
        u_min = u_k_1.vector().min()
        percent = (t/T)*100

        if((percent-old_percent)>=CONST.PERCENT_STEP):
            print CONST.PERCENT_OUTPUT %(percent,t/100,u_max,u_min)
            old_percent=percent

        t+=delta_t
        u_k_1.assign(u_k)



    #save result
    #u_file = CONST.LQR_MAT_FILENAME %(str(mesh.num_vertices())+'_'+integrator+'_'+C_func.__name__)
    #savemat(u_file, mdict={'u_end':u_k.vector().array(), 'T':T, 'delta_t':delta_t, 'integrator':integrator, 'Q':Q, 'R':R, 'C':C, 'C_name':C_func.__name__ })


if __name__=='__main__':
    LQR(CONST.MACRO_MESH_FILE, CONST.MACRO_GAMMA_NUMBERS_FILE, CONST.EULER_IMPLICIT, CONST.LQR_C_60, CONST.LQR_R, CONST.LQR_Q)
    LQR(CONST.MACRO_MESH_FILE, CONST.MACRO_GAMMA_NUMBERS_FILE, CONST.EULER_IMPLICIT, CONST.LQR_C_DIFF, CONST.LQR_R, CONST.LQR_Q)
    LQR(CONST.MACRO_MESH_FILE, CONST.MACRO_GAMMA_NUMBERS_FILE, CONST.EULER_IMPLICIT, CONST.LQR_C_TEMP, CONST.LQR_R, CONST.LQR_Q)


