import numpy as np
from dolfin import *
from dolfin_utils import *
from dolfin import MeshFunction
import constants as CONST
from constants import EDGE_INNER




mesh            = dolfin.Mesh(CONST.MACRO_MESH_FILE)
#read edge numbers MeshFunction from file and compute gamma numbers MeshFunction
edge_numbers    = dolfin.MeshFunction('size_t',mesh,CONST.MACRO_EDGE_NUMBERS_FILE)


dummy_marker = dolfin.MeshFunction('bool',mesh,mesh.topology().dim(),True)


parameters['refinement_algorithm']='bisection'
print parameters['refinement_algorithm']
bis_mesh = refine(mesh)
File('bis_mesh.pvd')<<bis_mesh

#adabis_mesh = adapt(mesh)
#File('adabis_mesh.pvd')<<adabis_mesh
#adabis_edge_numbers = adapt(edge_numbers,bis_mesh)
#File('adabis_edge_numbers.pvd')<<adabis_edge_numbers


#######
parameters['refinement_algorithm']='recursive_bisection'
print parameters['refinement_algorithm']
recbis_mesh = refine(mesh,dummy_marker)
#    parent_cell (size = 379)
#    parent_facet (size = 607)
info(recbis_mesh,True)
File('recbis_mesh.pvd')<<recbis_mesh

#adarecbis_mesh = adapt(mesh,dummy_marker)
#File('adarecbis_mesh.pvd')<<adarecbis_mesh
#adarecbis_edge_numbers = adapt(edge_numbers,recbis_mesh)
#File('adarecbis_edge_numbers.pvd')<<adarecbis_edge_numbers




#######
parameters['refinement_algorithm']='iterative_bisection'
print parameters['refinement_algorithm']
itbis_mesh = refine(mesh)
info(itbis_mesh,True)
File('itbis_mesh.pvd')<<itbis_mesh


#adaitbis_mesh = adapt(mesh)
#File('adaitbis_mesh.pvd')<<adaitbis_mesh
#adaitbis_edge_numbers = adapt(edge_numbers,adaitbis_mesh)
#File('adaitbis_edge_numbers.pvd')<<adaitbis_edge_numbers

#######
parameters['refinement_algorithm']='regular_cut'
print parameters['refinement_algorithm']
regcut_mesh = refine(mesh,dummy_marker)
info(regcut_mesh.data(),True)
File('regcut_mesh.pvd')<<regcut_mesh

#adaregcut_mesh = adapt(mesh)
#File('adaregcut_mesh.pvd')<<adaregcut_mesh
#adaregcut_edge_numbers = adapt(edge_numbers,adaregcutbis_mesh)
#File('adaregcut_edge_numbers.pvd')<<adaregcut_edge_numbers




##regcut itbisection bisection machen das gleiche
#recbis mach bisection


#was ist der unterschied zwischen adapt und refine?????

