import numpy as np
import constants as CONST
from dolfin import *
from dolfin_utils import *
from dolfin.cpp.la import uBLASDenseMatrix
from pycmess import lrnm, equation, options
from scipy.linalg import cholesky, solve_triangular, cho_solve, norm
from scipy.io import savemat
from scipy.sparse import csr_matrix

#set la backend
parameters['linear_algebra_backend'] = 'uBLAS'


#meshfile and boundary parts
mesh_file = CONST.MACRO_MESH_FILE
gamma_numbers_file = CONST.MACRO_GAMMA_NUMBERS_FILE



#read mesh file and mesh function for gamma boundary
mesh = dolfin.Mesh(mesh_file)
gamma_numbers = dolfin.MeshFunction('size_t',mesh,gamma_numbers_file)

#switch off dof reordering
parameters['reorder_dofs_serial']=False
V_dof_off               = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)
u_dof_off               = TrialFunction(V_dof_off)
v_dof_off               = TestFunction(V_dof_off)
S_var_dof_off           = inner(grad(u_dof_off),grad(v_dof_off))*dx
S_dof_off               = assemble(S_var_dof_off)


#switch on dof reordering
parameters['reorder_dofs_serial']=True
V_dof_on                = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)
u_dof_on                = TrialFunction(V_dof_on)
v_dof_on                = TestFunction(V_dof_on)
S_var_dof_on            = inner(grad(u_dof_on),grad(v_dof_on))*dx
S_dof_on                = assemble(S_var_dof_on)


#should be different because of reordering
print norm(S_dof_off.array()-S_dof_on.array())

#should be the identity
print '---dof_off---'
print CONST.get_dof_to_vertex_map(V_dof_off, mesh)
print CONST.get_vertex_to_dof_map(V_dof_off, mesh)


#should give a permutation and its inverse
print '---dof_on---'
print CONST.get_dof_to_vertex_map(V_dof_on,mesh)
print CONST.get_vertex_to_dof_map(V_dof_on,mesh)
dof_to_vertex=CONST.get_dof_to_vertex_map(V_dof_on,mesh)
vertex_to_dof=CONST.get_vertex_to_dof_map(V_dof_on,mesh)


#order dof_on data  to dof_off data
tempS = np.zeros((mesh.num_vertices(), mesh.num_vertices()))
tempS2 = np.zeros((mesh.num_vertices(), mesh.num_vertices()))
tempS[dof_to_vertex,:] = S_dof_on.array()
tempS2[:,dof_to_vertex] = tempS[:]
print norm(tempS2-S_dof_off.array())



# check multiplication with C
C_dof_on = CONST.LQR_C_60(mesh.num_vertices(),vertex_to_dof)
C_dof_off = CONST.LQR_C_60(mesh.num_vertices(),CONST.get_vertex_to_dof_map(V_dof_off,mesh)) #identity!


temp1 = np.dot(S_dof_on.array(),C_dof_on.toarray().T)
temp2 =temp1.copy()
temp2[dof_to_vertex,:]=temp1[:]

#should be zero because after reordering equals
print np.linalg.norm(np.dot(S_dof_off.array(),C_dof_off.toarray().T)-    temp2 )
















