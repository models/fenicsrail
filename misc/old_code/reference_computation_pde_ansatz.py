
def PDE_ansatz_linear(mesh_file, gamma_numbers_file,integrator):
    """Reference computation (4.2 Die ungesteuerte Referenzrechnung) p.47. via with PDE-Ansatz.

    Function takes the constants from wip_constants, define a weak formulation and solve it with PDE-Ansatz
    Use constant coefficents rho, c, lambda.

    Function takes the constants from wip_constants, define a weak formulation and solve it with PDE-Ansatz and numerical integration.
    Save temperature in vtk file and temperature at the end in mat file see constants.py

     Args:
        mesh_file           (String):        points to the .xml mesh file (mesh folder)
        gamma_numbers_file  (String):        points to the .xml MeshFunction file (mesh folder)
        integrator          (String):        CONST.EULER_IMPLICIT, CONST.EULER_EXPLICIT, CONST.CRANK_NICOLSON

    Returns:

    Raises:

    Information:

    """

    #set constants
    const_rho       =   Constant(CONST.CONST_RHO)
    const_c         =   Constant(CONST.CONST_C)
    const_kappa     =   Constant(CONST.CONST_KAPPA)
    const_alpha     =   Constant(CONST.CONST_ALPHA)
    const_u_ext     =   Constant(CONST.CONST_UEXT)

    #read mesh and gamma_numbers
    mesh            =   Mesh(mesh_file)
    gamma_numbers   =   MeshFunction('size_t', mesh, gamma_numbers_file)

    #define function space linear Lagrange P1
    V               =   FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    #set time step size and simulation time and t for iterating over time
    delta_t         =   CONST.T_DELTA
    T               =   CONST.T_END
    t               =   delta_t

    #the solution u^{k}
    u_k             =   TrialFunction(V)
    v               =   TestFunction(V)

    #define u0 as constant temperature
    u_0             =   Constant(CONST.CONST_U0)

    #project u_0 down to Function Space V
    u_k_1           =   project(u_0,V)

    ds = Measure('ds')[gamma_numbers]
    if integrator==CONST.EULER_IMPLICIT:
        #define bilinear form
        a           =   u_k*v*dx + delta_t*const_alpha*inner(grad(u_k),grad(v))*dx
        a           +=  sum([delta_t*const_kappa/(const_c*const_rho) * u_k*v *ds(i) for i in range(1,8)])

        #define linear form (right hand side)
        L           =   u_k_1*v*dx
        L           +=  sum( [delta_t*const_kappa/(const_c*const_rho)*const_u_ext*v*ds(i) for i in range(1,8)])
    elif integrator==CONST.EULER_EXPLICIT:
        #define bilinear form
        a           =   u_k*v*dx

        #define linear form (right hand side)
        L           =   u_k_1*v*dx - delta_t*const_alpha*inner(grad(u_k_1),grad(v))*dx
        L           +=  sum([delta_t*(const_kappa/(const_c*const_rho))*(const_u_ext-u_k_1)*v*ds(i) for i in range(1,8)])

    elif integrator == CONST.CRANK_NICOLSON:
        #define bilinear form
        a           =   u_k*v*dx + delta_t*const_alpha*0.5*inner(grad(u_k),grad(v))*dx
        a           +=  sum([0.5*delta_t*const_kappa/(const_c*const_rho) * u_k*v *ds(i)     for i in range(1,8)])

        #define linear form (right hand side)
        L           =   u_k_1*v*dx-delta_t*0.5*const_alpha*inner(grad(u_k_1),grad(v))*dx
        L           +=  sum([delta_t*(const_kappa/(const_c*const_rho))*const_u_ext*v*ds(i)  for i in range(1,8)])
        L           -=  sum([0.5*delta_t*(const_kappa/(const_c*const_rho))*u_k_1*v*ds(i)    for i in range(1,8)])

    else:
        raise ValueError('integration method: %s is not implemented'% (integrator))


    #the solution
    u_k             =   Function(V)

    #set name in vtk file
    u_k_1.rename(CONST.VTK_FIELD_NAME,CONST.VTK_LABEL_NAME)

    #file for visualization
    vtk_file        =   File(CONST.PDE_ANSATZ_LINEAR_VTK_FILENAME %(str(mesh.num_vertices())+'_'+integrator),'compressed')
    vtk_file << u_k_1

    #get logger
    add_file_handler(CONST.PDE_ANSATZ_LINEAR_LOG % (str(mesh.num_vertices())+'_'+integrator) )
    raillogger.log(railloglevel,"mesh_file=%s"%mesh_file)
    raillogger.log(railloglevel,"gamma_numbers_file=%s"%gamma_numbers_file)
    raillogger.log(railloglevel,"integrator=%s"%integrator)

    #debug info
    old_percent     =   -CONST.PERCENT_STEP
    #time between two vtk sample
    old_time        =   -CONST.VTK_SAMPLE_TIME


    while t<=T:
        solve(a==L,u_k)

        #check time differenz for vtk sample
        if(t-old_time)>=CONST.VTK_SAMPLE_TIME:
            raillogger.log(railloglevel,"Write VTK Dump at time:%.2fs"%(t/100))
            vtk_file << u_k
            old_time=t

        #check progress for output information
        percent = (t/T)*100
        if((percent-old_percent)>=CONST.PERCENT_STEP):
            raillogger.log(railloglevel,CONST.PERCENT_OUTPUT %(percent,t/100,u_k.vector().max(),u_k.vector().min()))
            old_percent=percent

        t+=delta_t
        u_k_1.assign(u_k)

    raillogger.log(railloglevel,CONST.PERCENT_OUTPUT %(percent,t/100,u_k.vector().max(),u_k.vector().min()))

    #save result
    u_file          =   CONST.PDE_ANSATZ_LINEAR_MAT_FILENAME %(str(mesh.num_vertices())+'_'+integrator)
    savemat(u_file, mdict={'u_end':u_k.vector().array(),'T':T,'delta_t':delta_t, 'integrator':integrator, \
                           'doftovertexmap':CONST.get_dof_to_vertex_map(V,mesh), 'vertextodofmap':CONST.get_vertex_to_dof_map(V,mesh)})

    raillogger.log(railloglevel,'Save data in %s'% u_file )
    #remove file_handler from railloger
    remove_file_handlers_handlers()


def PDE_ansatz_nonlinear(mesh_file, gamma_numbers_file,integrator):
    """Reference computation (4.2 Die ungesteuerte Referenzrechnung) p.47. via with PDE-Ansatz with nonlinear coefficients

    Function takes the constants from wip_constants, define a weak formulation and solve it with PDE-Ansatz
    Use linear coefficents rho, c, lambda.

    Function takes the constants from wip_constants, define a weak formulation and solve it with PDE-Ansatz and numerical integration.
    Save temperature in vtk file and temperature at the end in mat file see constants.py

     Args:
        mesh_file           (String):        points to the .xml mesh file (mesh folder)
        gamma_numbers_file  (String):        points to the .xml MeshFunction file (mesh folder)
        integrator          (String):        CONST.EULER_IMPLICIT, CONST.EULER_EXPLICIT, CONST.CRANK_NICOLSON

    Returns:

    Raises:

    Information:

    """

    #set constants
    rho             =   CONST.LINEAR_RHO
    c               =   CONST.LINEAR_C
    const_kappa     =   Constant(CONST.CONST_KAPPA)
    alpha           =   CONST.LINEAR_ALPHA
    const_u_ext     =   Constant(CONST.CONST_UEXT)

    #read mesh and gamma_numbers
    mesh            =   Mesh(mesh_file)
    gamma_numbers   =   MeshFunction('size_t', mesh, gamma_numbers_file)

    #define function space linear Lagrange P1
    V               =   FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)

    #set time step size and simulation time and t for iterating over time
    delta_t         =   CONST.T_DELTA
    T               =   CONST.T_END
    t               =   delta_t

    #the solution u^{k}
    u_k             =   TrialFunction(V)
    v               =   TestFunction(V)

    #define u0 as constant temperature
    u_0             =   Constant(CONST.CONST_U0)

    #project u_0 down to Function Space V
    u_k_1           =   project(u_0,V)

    ds = Measure('ds')[gamma_numbers]
    if integrator==CONST.EULER_IMPLICIT:
        #define bilinear form
        a           =   u_k*v*dx + delta_t*alpha(u_k_1)*inner(grad(u_k),grad(v))*dx
        a           +=  sum([delta_t*const_kappa/(c(u_k_1)*rho(u_k_1)) * u_k*v *ds(i) for i in range(1,8)])

        #define linear form (right hand side)
        L           =   u_k_1*v*dx
        L           +=  sum( [delta_t*const_kappa/(c(u_k_1)*rho(u_k_1))*const_u_ext*v*ds(i) for i in range(1,8)])
    elif integrator==CONST.EULER_EXPLICIT:
        #define bilinear form
        a           =   u_k*v*dx

        #define linear form (right hand side)
        L           =   u_k_1*v*dx - delta_t*alpha(u_k_1)*inner(grad(u_k_1),grad(v))*dx
        L           +=  sum([delta_t*(const_kappa/(c(u_k_1)*rho(u_k_1)))*(const_u_ext-u_k_1)*v*ds(i) for i in range(1,8)])

    elif integrator == CONST.CRANK_NICOLSON:
        #define bilinear form
        a           =   u_k*v*dx + delta_t*alpha(u_k_1)*0.5*inner(grad(u_k),grad(v))*dx
        a           +=  sum([0.5*delta_t*const_kappa/(c(u_k_1)*rho(u_k_1)) * u_k*v *ds(i)     for i in range(1,8)])

        #define linear form (right hand side)
        L           =   u_k_1*v*dx-delta_t*0.5*alpha(u_k_1)*inner(grad(u_k_1),grad(v))*dx
        L           +=  sum([delta_t*(const_kappa/(c(u_k_1)*rho(u_k_1)))*const_u_ext*v*ds(i)  for i in range(1,8)])
        L           -=  sum([0.5*delta_t*(const_kappa/(c(u_k_1)*rho(u_k_1)))*u_k_1*v*ds(i)    for i in range(1,8)])

    else:
        raise ValueError('integration method: %s is not implemented'% (integrator))


    #the solution
    u_k             =   Function(V)

    #set name in vtk file
    u_k_1.rename(CONST.VTK_FIELD_NAME,CONST.VTK_LABEL_NAME)


    #file for visualization
    vtk_file        =   File(CONST.PDE_ANSATZ_NONLINEAR_VTK_FILENAME %(str(mesh.num_vertices())+'_'+integrator),'compressed')
    vtk_file << u_k


    #debug info
    old_percent     =   -CONST.PERCENT_STEP
    #time between two vtk sample
    old_time        =   -CONST.VTK_SAMPLE_TIME


    #get logger
    add_file_handler(CONST.PDE_ANSATZ_NONLINEAR_LOG % (str(mesh.num_vertices())+'_'+integrator) )
    raillogger.log(railloglevel,"mesh_file=%s"%mesh_file)
    raillogger.log(railloglevel,"gamma_numbers_file=%s"%gamma_numbers_file)
    raillogger.log(railloglevel,"integrator=%s"%integrator)

    while t<=T:
        solve(a==L,u_k)
        #check time differenz for vtk sample
        if(t-old_time)>=CONST.VTK_SAMPLE_TIME:
            raillogger.log(railloglevel,"Write VTK Dump at time:%.2fs"%(t/100))
            vtk_file << u_k
            old_time=t

        #check progress for output information
        percent = (t/T)*100
        if((percent-old_percent)>=CONST.PERCENT_STEP):
            raillogger.log(railloglevel,CONST.PERCENT_OUTPUT %(percent,t/100,u_k.vector().max(),u_k.vector().min()))
            old_percent=percent

        t+=delta_t
        u_k_1.assign(u_k)

    raillogger.log(railloglevel,CONST.PERCENT_OUTPUT %(percent,t/100,u_k.vector().max(),u_k.vector().min()))

    #save result
    u_file          =   CONST.PDE_ANSATZ_NONLINEAR_MAT_FILENAME %(str(mesh.num_vertices())+'_'+integrator)
    savemat(u_file, mdict={'u_end':u_k.vector().array(),'T':T,'delta_t':delta_t, 'integrator':integrator, \
                           'doftovertexmap':CONST.get_dof_to_vertex_map(V,mesh), 'vertextodofmap':CONST.get_vertex_to_dof_map(V,mesh)})

    raillogger.log(railloglevel,'Save data in %s'% u_file )
    #remove file_handler from railloger
    remove_file_handlers_handlers()
