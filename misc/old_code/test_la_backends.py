import numpy as np
import constants as CONST
import dolfin
from dolfin import *
from dolfin_utils import *
from dolfin.cpp.la import uBLASDenseMatrix
from pycmess import lrnm, equation, options
from scipy.linalg import cholesky, solve_triangular, cho_solve
from scipy.io import savemat
from scipy.sparse import csr_matrix
import os
import pdb





#read mesh and gamma_numbers
mesh            = dolfin.Mesh(CONST.MACRO_MESH_FILE)

#define function space linear Lagrange P1
V               = FunctionSpace(mesh, CONST.FUNCTION_SPACE, CONST.FUNCTION_DEGREE)


#the solution u and the derivative du_dt
u               = TrialFunction(V)
du_dt           = TrialFunction(V)
v               = TestFunction(V)

#define terms of weak forumlations
M_var           = du_dt*v*dx


#linear_algebra_backend   |  string    STL        [Epetra, PETSc, STL, uBLAS]

#assemble matrices
parameters['linear_algebra_backend']='STL'
M_STL               = assemble(M_var)

#parameters['linear_algebra_backend']='Epetra'
#M_Epetra               = assemble(M_var)

parameters['linear_algebra_backend']='PETSc'
M_PETSc               = assemble(M_var)

parameters['linear_algebra_backend']='uBLAS'
M_uBLAS               = assemble(M_var)


