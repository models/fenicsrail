function ret = meshconv(element_boundaries,element_vertices)


for i = 1:size(element_boundaries,1)    
[sorted,ix] =sort(element_vertices(i,:));
sortedbound = element_boundaries(i,ix);

out='<value cell_index="%d" local_entity="%d" value="%d" />\n';

for j = 0:2
fprintf(out,i-1,j,abs(sortedbound(j+1)))
end




end
